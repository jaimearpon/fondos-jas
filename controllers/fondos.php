<?php

$redirect_map = array(
	'/capital/' => '/afp-capital/',
	'/cuprum/' => '/afp-cuprum/',
	'/habitat/' => '/afp-habitat/',
	'/modelo/' => '/afp-modelo/',
	'/planvital/' => '/afp-planvital/',
	'/provida/' => '/afp-provida/',
);

foreach ($redirect_map as $old_url => $new_url) {
	if (strpos($_SERVER['REQUEST_URI'], $old_url) !== FALSE ) {
		$_new_url = str_replace($old_url, $new_url, $_SERVER['REQUEST_URI']);
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: http://".$_SERVER['HTTP_HOST']."$_new_url");
		exit;
	}
}

$uri = substr($_SERVER['REQUEST_URI'], 1);
$uri = substr($uri, 0, -1);
$parts = explode("/", $uri);


$afp_url_map = array(
	'afp-capital' => 'AFP Capital',
	'afp-cuprum' => 'AFP Cuprum',
	'afp-habitat' => 'AFP Habitat',
	'afp-modelo' => 'AFP Modelo',
	'afp-planvital' => 'AFP Planvital',
	'afp-provida' => 'AFP Provida',
);
$afp_api_map = array(
	'afp-capital' => 'CAPITAL',
	'afp-cuprum' => 'CUPRUM',
	'afp-habitat' => 'HABITAT',
	'afp-modelo' => 'MODELO',
	'afp-planvital' => 'PLANVITAL',
	'afp-provida' => 'PROVIDA',
);
$fondo_url_map = array(
	'fondo-a' => 'A',
	'fondo-b' => 'B',
	'fondo-c' => 'C',
	'fondo-d' => 'D',
	'fondo-e' => 'E',
);


$afp_name = '';
$fondo_name = '';
$afp_api_name = '';

if (count($parts) == 1) {
	$afp_name     = $afp_url_map['afp-habitat'];
	$fondo_name   = '';
	$afp_api_name = $afp_api_map['afp-habitat'];
} elseif (count($parts) == 2 ) {
	$afp_name     = $afp_url_map[$parts[1]];
	$fondo_name   = '';
	$afp_api_name = $afp_api_map[$parts[1]];
} elseif (count($parts) == 3) {
	$afp_name     = $afp_url_map[$parts[1]];
	$fondo_name   = $fondo_url_map[$parts[2]];
	$afp_api_name = $afp_api_map[$parts[1]];
}

$fondostitle = array(
	'afp' => $afp_name,
	'fondo' => $fondo_name,
	'titulo' => 'Rentabilidad de los fondos de pensión de las AFP - Fondos AFP'
);

$details = array();
$details['fondos'] = array();

$page = array('selected' => 'fondos');

$aAFPs = AFP::getAFPs();
$afps = array();
$afp_id = 0;
foreach ( $aAFPs as $afp ) {
	$afps[$afp->getAPIName()] = 'AFP ' . ucfirst(strtolower($afp->getNombre()));
	if ( $afp->getAPIName() == $afp_api_name ) {
		$afp_id = $afp->getID();
		$details['afp'] = $afp->getNombre();
	}
}
$smarty->assign('afps', $afps);

//Fondos disponibles
$fondos = array('A'=>'A','B'=>'B','C'=>'C','D'=>'D','E'=>'E');
$smarty->assign('fondos', $fondos);


//Fondos seleccionados
$defaults = array('A', 'E');
if (strlen($fondo_name) > 0) {
	$defaults = array($fondo_name);
}
$default_fondos = Fondo::getDefaultFondos($defaults);
$fondos_ids = array();
foreach ($default_fondos as $default_fondo) {
	$fondos_ids[$default_fondo] = $default_fondo;
}
$smarty->assign('fondos_ids', $fondos_ids);


//Fechas de búsqueda
$desde = isset($_POST['desde']) ? $_POST['desde'] : '';
$hasta = isset($_POST['hasta']) ? $_POST['hasta'] : '';
$d = gmdate("Y-m-d", strtotime("-6 months"));
$h = gmdate("Y-m-d", strtotime("now"));
if ( strlen($desde) ) $d = $desde;
if ( strlen($hasta) ) $h = $hasta;
$smarty->assign('desde', $d);
$smarty->assign('hasta', $h);
$details['periodo'] = "$d al $h";

//Tipo de gráfico
$tipo = isset($_POST['tipo']) ? $_POST['tipo'] : 'valor';
if (!in_array($tipo, array('valor', 'porcentaje', 'patrimonio'))) {$tipo = 'valor';}
$smarty->assign('tipo',$tipo);

$smarty->assign('afp_id', $afp_id);
$smarty->assign('afp_api_name', $afp_api_name);

$ocultarFDS = isset($_POST['ocultarFDS']);

//$imagen = Grafico::crearGraficoFondos($afp_id, $fondos, $d, $h, $details, $tipo, $ocultarFDS);
//$fondos_track = "_gaq.push(['_trackPageview', '/fondos/$d/$h/$afp_id/".implode(",", $fondos)."/$tipo']);";
//$smarty->assign(compact('fondos_track'));

$page['description'] = "Rentabilidad de los fondos A, B, C, D y E de la AFPs de Chile";
if(strlen($fondostitle['afp']) && strlen($fondostitle['fondo']) == 0) {
	$fondostitle['titulo'] = "Rentabilidad de los fondos de pensión de la $afp_name - Fondos AFP";
	$page['description'] = "Rentabilidad de los fondos de pensión de la $afp_name";
} elseif (strlen($fondostitle['afp']) && strlen($fondostitle['fondo'])) {
	$fondostitle['titulo'] = "Rentabilidad del fondo $fondo_name de la $afp_name - Fondos AFP";
	$page['description'] = "Rentabilidad del fondo $fondo_name de la $afp_name ";
}

$smarty->assign('title', $fondostitle['titulo']);
$smarty->assign('mtime_js_commons', filemtime(dirname(dirname(__FILE__)).DS.'webroot'.DS.'assets'.DS.'js'.DS.'common.js'));
$smarty->assign('mtime_js_fondos', filemtime(dirname(dirname(__FILE__)).DS.'webroot'.DS.'assets'.DS.'js'.DS.'fondos.js'));
$smarty->assign('mtime_css_main', filemtime(dirname(dirname(__FILE__)).DS.'webroot'.DS.'assets'.DS.'css'.DS.'main.css'));
$smarty->assign(compact('ocultarFDS', 'fondostitle', 'details', 'page'));
$smarty->assign('query_count', Database::getInstance()->getQueryCount());
$smarty->assign('elapsed_time', Database::getInstance()->getElapsedTime());
$smarty->display('fondos.tpl');
