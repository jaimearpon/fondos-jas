<?php
$smarty->assign('page', array(
	'selected' => 'about'
));
$smarty->assign('title', 'Quienes somos - FondosAFP');
$smarty->assign('mtime_js_commons', filemtime(dirname(dirname(__FILE__)).DS.'webroot'.DS.'assets'.DS.'js'.DS.'common.js'));
$smarty->assign('mtime_css_main', filemtime(dirname(dirname(__FILE__)).DS.'webroot'.DS.'assets'.DS.'css'.DS.'main.css'));
$smarty->assign('query_count', Database::getInstance()->getQueryCount());
$smarty->assign('elapsed_time', Database::getInstance()->getElapsedTime());
$smarty->display('about.tpl');