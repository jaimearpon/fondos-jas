<?php

try {
	$alerta = Alerta::crearAlerta($_POST);
	echo json_encode(array(
		'result' => $alerta ? 'OK' : 'INFO', 
		'message' => $alerta ? 'Alerta creada!' : 'Alerta creada anteriormente' 
	));
} catch (Exception $ex ) {
	Log::getInstance()->logException($ex);
	echo json_encode(array(
		'result' => 'ERROR',
		'error' => $ex->getMessage()
	));
}
