<?php

$error = '';
if ( isset($_POST) && count($_POST) > 0 ) {
	$nombre = htmlentities(utf8_decode($_POST['nombre']));
	$telefono = htmlentities(utf8_decode($_POST['telefono']));
	$email = $_POST['email'];
	$consulta = htmlentities(utf8_decode($_POST['consulta']));
	
	if ( strlen($nombre) == 0 )
                $error .= "Por favor ingrese su nombre".PHP_EOL;
	if ( strlen($email) == 0 || filter_var($email, FILTER_VALIDATE_EMAIL) === false )
                $error .= "Por favor ingrese un email válido".PHP_EOL;
        if ( strlen($consulta) == 0 )
                $error .= "Por favor ingrese su comentario".PHP_EOL;
        
	if (strlen($error) == 0 ) {
		//TODO: buscar una forma de agregar los headers al email a través de Amazon SES
//		$headers = "From: FondosAFP <".AWS_EMAIL_CONTACTO."> \r\n";
//		$headers .= "Reply-to: $email\r\n";
//		$headers .= "Content-Type: text/html";
		
		$to = 'contacto@fondosafp.cl';
		$subject = "$nombre se ha contactado desde http://" . $_SERVER['SERVER_NAME'];
		$content = "<p>$consulta</p><p>Telefono: $telefono</p><p>Email: $email</p>";
		AmazonSES::getInstance()->sendEmail($to, AWS_EMAIL_CONTACTO, $subject, $content);
		
//		$headers2 = "From: FondosAFP <".AWS_EMAIL_CONTACTO."> \r\n";
//		$headers2 .= "Content-Type: text/html";
		$subject = "Gracias por contactarse con FondosAFP";
		$content = "<p>Hola $nombre!</p><p>Gracias por contactarte con nosotros en FondosAFP, hemos recibido tu mensaje.</p><p>Tu mensaje: \"$consulta\"</p><br/><p>--<br/>FondosAFP<br/>Santiago, Chile<br/>contacto@fondosafp.cl</p>";
		AmazonSES::getInstance()->sendEmail($email, AWS_EMAIL_CONTACTO, $subject, $content);
		$smarty->assign('page', array('selected' => 'contacto'));
		$smarty->display('contacto_success.tpl');
		exit;
	}
}

$smarty->assign('page', array(
	'selected' => 'contacto',
	'styles' => array(
	    'style.css'
	),
	'description' => 'Contáctate con nosotros'
));
$smarty->assign('error', nl2br($error));
$smarty->assign('mtime_js_commons', filemtime(dirname(dirname(__FILE__)).DS.'webroot'.DS.'assets'.DS.'js'.DS.'common.js'));
$smarty->assign('mtime_css_main', filemtime(dirname(dirname(__FILE__)).DS.'webroot'.DS.'assets'.DS.'css'.DS.'main.css'));
$smarty->assign('query_count', Database::getInstance()->getQueryCount());
$smarty->assign('elapsed_time', Database::getInstance()->getElapsedTime());
$smarty->display('contacto.tpl');