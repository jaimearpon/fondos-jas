<?php

$smarty->assign('page', array(
	'styles' => array(
	    'style.css'
	)
));
header('HTTP/1.0 404 Not Found');
$smarty->assign('query_count', Database::getInstance()->getQueryCount());
$smarty->assign('elapsed_time', Database::getInstance()->getElapsedTime());
$smarty->display('404.tpl');
