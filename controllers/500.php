<?php
$smarty->assign('page', array(
	'styles' => array(
	    'style.css'
	)
));

$message = $smarty->getVariable('error');
$trace = $smarty->getVariable('trace');
$html = "<p>Ha ocurrido un error en el sitio web, aqui el detalle:</p>";
$html .= "<p>Mensaje: $message</p><p>Trace: $trace</p>";
$html .= "<p>URL: {$_SERVER['REQUEST_URI']}<p/>";

$amazonSES = AmazonSES::getInstance();
$amazonSES->sendEmail('julio.araya@fondosafp.cl', AWS_EMAIL_CONTACTO, 'Error 500', $html);

header('HTTP/1.0 500 Server Error');
$smarty->assign('query_count', Database::getInstance()->getQueryCount());
$smarty->assign('elapsed_time', Database::getInstance()->getElapsedTime());
$smarty->display('500.tpl');
