<?php

$blog = array(
	'info' => Blog::getBlogInfo()
);

$url = $_SERVER['REQUEST_URI'];
if (strpos($url, 'post/')) {
	$new_url = str_replace('post/', '', $url);
	header('Location: http://www.fondosafp.cl'. $new_url);
	exit;
}

$request_uri = $_SERVER['REQUEST_URI'];
$parts = explode('/', $request_uri);

if (array_key_exists(2, $parts) && strlen($parts[2]) && $parts[2] == 'page') {
	$page = (int)$parts[3];
	if ($page == 1) {
		header('Location: ' . SERVER_URL . 'blog/');
		exit();
	}
	if ($page > 0) {
		$blog['posts'] = Blog::getPosts($page);
		$blog['page'] = $page;
	}
} elseif (array_key_exists(2, $parts) && strlen($parts[2]) && (int)$parts[2] > 0) {
	$blog['posts']  = Blog::getPost($parts[2]);
	$blog['single'] = true;
} else {
	$blog['posts'] = Blog::getPosts();
	$blog['page'] = 1;
}

$smarty->assign('blog', $blog);

$smarty->assign('page', array(
	'selected' => 'blog',
	'styles' => array(
		'style.css'
	),
	'description' => 'Blog - Fondos AFP'
));

$smarty->assign('mtime_js_commons', filemtime(dirname(dirname(__FILE__)).DS.'webroot'.DS.'assets'.DS.'js'.DS.'common.js'));
$smarty->assign('mtime_css_main', filemtime(dirname(dirname(__FILE__)).DS.'webroot'.DS.'assets'.DS.'css'.DS.'main.css'));
$smarty->assign('query_count', Database::getInstance()->getQueryCount());
$smarty->assign('elapsed_time', Database::getInstance()->getElapsedTime());
$smarty->display('blog.tpl');