<?php

$uri = substr($_SERVER['REQUEST_URI'],1,-1);

list($ajax, $action) = explode("/", $uri);


if ( $action ) {
	$file = dirname(__FILE__).DS.'ajax'.DS.$action.'.php';
	//Log::getInstance()->log("accion $uri: $file");
	if ( file_exists($file) ){
		require_once($file);
		exit;
	}
	
}
require_once('404.php');