<?php
ob_clean();
ob_start();
$url = explode('/', $_SERVER['REQUEST_URI']);
unset($url[0]);
if(count($url) > 1) {
	unset($url[1]);
}
$url_parts = array_values($url);
if (count($url_parts) == 0 || $url_parts[0] !== '1.0') {
	header('Location: ' . SERVER_URL .'api/1.0/');
	exit;
}

$version = $url_parts[0];
$endpoint = $url_parts[1];

if (strpos($endpoint, '?')) {
	$endpoint = substr($endpoint, 0, strpos($endpoint, '?'));
}

$response = API::getInstance()->processRequest($version, $endpoint, $_GET, $_POST, $_SERVER);
header('Content-length: ' . strlen($response));
echo $response;
ob_flush();