<?php

$smarty->assign('page', array(
	'selected' => 'alertas',
	'styles' => array(
	    'style.css'
	),
	'description' => 'Recibe alertas por correo cuando cambie el valor cuota de tus fondos'
));

$aAFPs = AFP::getAFPs();
$afps = array();
foreach ( $aAFPs as $afp ) {
	$afps[$afp->getID()] = 'AFP ' . ucfirst(strtolower($afp->getNombre()));
}
$smarty->assign('afps', $afps);

$aFondos = Fondo::getFondos();
$fondos = array();
foreach ( $aFondos as $fondo ) {
	$fondos[$fondo->getID()] = $fondo->getDescripcion();
}
$smarty->assign('fondos', $fondos);

$afp_id = AFP::getDefaultAFP();
if ( empty($afp_id))
	$afp_id = 1;
$smarty->assign('afp_id', $afp_id);

$fondo_id = Fondo::getDefaultFondo();
if ( empty($fondo_id) )
	$fondo_id = 1;

$ultimo_valor = array();
$uafps = array(1,2,3,4,5,6);
$ufondos = array(1,2,3,4,5);
foreach ( $uafps as $uafp){
	$ultimo_valor[$uafp] = array();
	foreach ( $ufondos as $ufondo ) {
		$valor = Cuota::getUltimoValor($uafp, $ufondo);
		$ultimo_valor[$uafp][$ufondo] = $valor['valor'];
	}		
}

$smarty->assign('ultimos_valores', json_encode($ultimo_valor));
$smarty->assign('fondo_id', $fondo_id);
$smarty->assign('mtime_js_commons', filemtime(dirname(dirname(__FILE__)).DS.'webroot'.DS.'assets'.DS.'js'.DS.'common.js'));
$smarty->assign('mtime_css_main', filemtime(dirname(dirname(__FILE__)).DS.'webroot'.DS.'assets'.DS.'css'.DS.'main.css'));
$smarty->assign('query_count', Database::getInstance()->getQueryCount());
$smarty->assign('elapsed_time', Database::getInstance()->getElapsedTime());
$smarty->display('alertas.tpl');
