<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"  xmlns:og="http://ogp.me/ns#" xmlns:fb="https://www.facebook.com/2008/fbml" itemscope itemtype="http://schema.org/Organization">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>{$title}</title>
	<meta name="description" content="{if isset($page.description)}{$page.description}{else}Informaci&oacute;n gratuita y actualizada de los fondos de pensiones de las principales AFPs de Chile.{/if}"/>
	<meta name="keywords" content="AFP, AFP Capital, AFP Provida, AFP Habitat, AFP Modelo, AFP Planvital, Grafico Fondos AFP, Fondos AFP, Habitat AFP"/> 
	<meta itemprop="name" content="FondosAFP" />
	<meta itemprop="description" content="{if isset($page.description)}{$page.description}{else}Informaci&oacute;n gratuita y actualizada de los fondos de pensiones de las principales AFPs de Chile.{/if}" />
	<meta itemprop="image" content="{$smarty.const.SERVER_URL}assets/images/logo.png" />
	<meta property="og:title" content="{$title}"/>
	<meta property="og:url" content="{$smarty.const.SERVER_URI}"/>
	<meta property="og:image" content="{$smarty.const.SERVER_URL}assets/images/logo.png"/>
	<meta property="og:type" content="non_profit" />
	<meta property="og:site_name" content="Fondos AFP" />
	<meta property="og:description" content="{if isset($page.description)}{$page.description}{else}Informaci&oacute;n gratuita y actualizada de los fondos de pensiones de las principales AFPs de Chile.{/if}" />
	<meta property="fb:admins" content="760428895" />
	<meta property="fb:app_id" content="666180683399525" />
	<link href="//{$smarty.server.HTTP_HOST}/assets/css/main.css" rel="stylesheet" type="text/css" />
	<link href="//{$smarty.server.HTTP_HOST}/assets/css/ui-lightness/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
	{if false}<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0-wip/css/bootstrap.min.css">{/if}
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	{if false}<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0-wip/js/bootstrap.min.js"></script>{/if}
	<script type='text/javascript' src='//{$smarty.server.HTTP_HOST}/assets/js/common.js'></script>
	<script type="text/javascript" src="https://apis.google.com/js/plusone.js">{ lang: 'es-419' }</script>
	{if isset($page.selected) && $page.selected=='fondos'}
	<script src="//{$smarty.server.HTTP_HOST}/assets/js/highstock/highstock.js"></script>
	<script src="//{$smarty.server.HTTP_HOST}/assets/js/highstock/modules/exporting.js"></script>
	<script src="//{$smarty.server.HTTP_HOST}/assets/js/fondos.js"></script>
	{/if}
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_LA/all.js#xfbml=1&appId=666180683399525";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="container-top">
	<div class="padding">
		<div class="top_message"><div></div></div>
		<h1 class="hide">FondosAFP - Información actualizada de fondos de pensión</h1>
		<a href="{$smarty.const.SERVER_URL}?ref=logo" class="logo"></a>
		<div class="global-nav">
			<ul>
				<li><a{if !isset($page.selected)} class="current"{/if} href="http://{$smarty.server.SERVER_NAME}/" title="Fondos AFP">Inicio</a></li>
				<li><a{if isset($page.selected) && $page.selected=='fondos'} class="current"{/if} title="Rentabilidad de los fondos de pensión de las AFP" href="http://{$smarty.server.SERVER_NAME}/fondos/">Fondos</a></li>
				<li><a{if isset($page.selected) && $page.selected=='alertas'} class="current"{/if} title="Alertas" href="http://{$smarty.server.SERVER_NAME}/alertas/">Alertas</a></li>
				<li><a{if isset($page.selected) && $page.selected=='blog'} class="current"{/if} title="Blog" href="http://{$smarty.server.SERVER_NAME}/blog/">Blog</a></li>
				{if false}
				<li><a{if isset($page.selected) && $page.selected=='contacto'} class="current"{/if} title="Contacto" href="http://{$smarty.server.SERVER_NAME}/contacto/">Contacto</a></li>
				<li><a{if isset($page.selected) && $page.selected=='about'} class="current"{/if} title="Quienes somos" href="http://{$smarty.server.SERVER_NAME}/about/">Quienes somos</a></li>
				{/if}
			</ul>
		</div>
	</div>
	<div id="social_menu">
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="https://www.fondosafp.com" data-text="@FondosAFP: Información gratuita y actualizada de los fondos de pensiones de Chile" data-lang="es" data-dnt="true">Twittear</a>
		<script>!function(d,s,id){ var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){ js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
		<div class="fb-like" data-href="https://www.facebook.com/pages/Fondos-AFP/549265641791345" data-send="false" data-layout="button_count" data-width="150" data-show-faces="false" data-font="lucida grande"></div>
	</div>
</div>
<!--BODY REGION-->
<div class="container-body">
