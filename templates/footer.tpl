</div><!--BODY REGION-->
<!--FOOT REGION-->
<div class="container-foot">
	<div class="padding">
		<div class="info">
			<h3>Fondos AFP</h3>
			<p> Santiago, Chile<br/>
			contacto [at] fondosafp.cl</p>
		</div>
		<div class="links">
			<ul class="foot">
				<li><a href="http://{$smarty.server.SERVER_NAME}/">Inicio</a></li>
				<li><a title="Fondos" href="http://{$smarty.server.SERVER_NAME}/fondos/">Fondos</a></li>
				<li><a title="Alertas" href="http://{$smarty.server.SERVER_NAME}/alertas/">Alertas</a></li>
				<li><a title="Alertas" href="http://{$smarty.server.SERVER_NAME}/blog/">Blog</a></li>
				{if false}
				<li><a title="Contacto" href="http://{$smarty.server.SERVER_NAME}/contacto/">Contacto</a></li>
				<li><a title="Quienes somos" href="http://{$smarty.server.SERVER_NAME}/about/">Quienes somos</a></li>
				{/if}
			</ul>
			&copy; 2011-2014 Fondos AFP.
		</div>
		<div class="clear"></div>
	</div>
</div>
{if $smarty.server.SERVER_NAME == 'www.fondosafp.com' }
	{if (isset($smarty.session.notrack)) }
<!-- _NT_ -->
	{else}
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){ i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-22286608-4', 'fondosafp.com');
  ga('send', 'pageview');
</script>
<script src="//static.getclicky.com/js" type="text/javascript"></script>
<script type="text/javascript">try{ clicky.init(100666504); }catch(e){}</script>
<noscript><p><img alt="Clicky" width="1" height="1" src="//in.getclicky.com/100666504ns.gif" /></p></noscript>
	{/if}
{else}
<pre>{$query_count} queries took {$elapsed_time} ms</pre>
{/if}
</body>
</html>
