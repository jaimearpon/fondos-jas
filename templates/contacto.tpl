{include file="header.tpl" title="Contacto - Fondos AFP"}
<div class="padding contacto">
    <h2>Contacto</h2>
    <!--main content-->
    <div class="main-content">
      <p>Envíanos tus comentarios, intentaremos responderte a la brevedad. </p>
      <p><span class="alert">*</span> <small>campos obligatorios.</small></p>
      <form action="{$smarty.const.SERVER_URL}contacto/" class="contact" method="post">
		{if strlen($error) > 0}<div class="error">{$error}</div>{/if}
		<fieldset>
			<label><span class="alert">*</span></label>
			<input name="nombre" type="text" class="element" id="nombre" placeholder="Nombre y apellido" />
			<div class="clear"></div>
			<label><span class="alert">*</span></label>
			<input name="email" type="text" class="element" id="email" placeholder="Correo electrónico" />
			<div class="clear"></div>
			<label>&nbsp;</label>
			<input name="telefono" type="text" class="element" id="telefono" placeholder="Teléfono de contacto" />
			<div class="clear"></div>
			<label><span class="alert">*</span></label>
			<textarea name="consulta" cols="15" rows="6" class="element large" id="consulta" placeholder="Escribe tu comentario" ></textarea>
			<div class="clear"></div>
			<input name="Submit" type="submit" class="button" value="Enviar" />
		</fieldset>
	</form>
    </div>
    <!--side bar-->
    <div class="side-content">
        <div class="page-flip">		
		<p><b>Redes sociales</b></p>
		<a href="https://twitter.com/FondosAFP" class="twitter-follow-button" data-show-count="false" data-lang="es" data-size="large" data-dnt="true">Seguir a @FondosAFP</a>
		<div class="fb-like-box" data-href="https://www.facebook.com/pages/Fondos-AFP/549265641791345" data-width="250" data-height="95" data-show-faces="false" data-stream="false" data-show-border="false" data-header="false"></div>
	</div>
    </div>
    <div class="clear"></div>
</div>
{if $smarty.server.SERVER_NAME == 'www.fondosafp.com' }
<img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6009878911351&amp;value=0" />
{/if}
{include file="footer.tpl"}