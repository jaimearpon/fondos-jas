{include file="header.tpl" title=$title}

<div class="padding">
	<h2>Quienes somos</h2>
	<p><a href="https://www.fondosafp.com" title="Fondos AFP">Fondos AFP</a> nació el año 2011 como una página de información básica en donde se pudieran
	apreciar mejor el comportamiento de los valores cuota de los fondos de pensión de las AFPs de Chile.
	Con el tiempo se agregaron mas medios de comunicación como <a href="http://blog.fondosafp.cl" target="_blank" title="Fondos AFP - Blog">Tumblr</a>, <a href="https://twitter.com/FondosAFP" target="_blank" title="Sigue a FondosAFP en Twitter">Twitter</a> y últimamente <a href="https://www.facebook.com/pages/Fondos-AFP/549265641791345" target="_blank" title="Ir a Fondos AFP en Facebook">Facebook</a> para
	desplegar los gráficos y noticias sobre el comportamiento de las bolsas mundiales.</p>

	<p><b>Jamás ha sido ni será nuestra intención cobrar por mostrar información que está disponible gratuitamente a través de la <a href="http://www.safp.cl" target="_blank" title="Valores de Cuota y Fondo / Superintendencia de Pensiones">Superintendencia de Pensiones</a></b></p>

	<ul>
		<li><a href="http://www.safp.cl/safpstats/stats/apps/vcuofon/vcfAFP.php?tf=A" target="_blank" title="Superintendencia de Pensiones - Fondo A">Superintendencia de Pensiones - Fondo A</a></li>
		<li><a href="http://www.safp.cl/safpstats/stats/apps/vcuofon/vcfAFP.php?tf=B" target="_blank" title="Superintendencia de Pensiones - Fondo B">Superintendencia de Pensiones - Fondo B</a></li>
		<li><a href="http://www.safp.cl/safpstats/stats/apps/vcuofon/vcfAFP.php?tf=C" target="_blank" title="Superintendencia de Pensiones - Fondo C">Superintendencia de Pensiones - Fondo C</a></li>
		<li><a href="http://www.safp.cl/safpstats/stats/apps/vcuofon/vcfAFP.php?tf=D" target="_blank" title="Superintendencia de Pensiones - Fondo D">Superintendencia de Pensiones - Fondo D</a></li>
		<li><a href="http://www.safp.cl/safpstats/stats/apps/vcuofon/vcfAFP.php?tf=E" target="_blank" title="Superintendencia de Pensiones - Fondo E">Superintendencia de Pensiones - Fondo E</a></li>
	</ul>
	
	<p>&nbsp;</p>
	<p class="team_member">
		<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
		<script type="IN/MemberProfile" data-id="http://www.linkedin.com/in/julioaraya" data-format="hover" data-text="Julio Araya" data-related="false"></script>
		/ <small>Fundador de Fondos AFP</small>
	</p>
	<div class="clear"></div>
</div>
{if $smarty.server.SERVER_NAME == 'www.fondosafp.com' }
<img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6009878828351&amp;value=0" />
{/if}
{include file="footer.tpl"}