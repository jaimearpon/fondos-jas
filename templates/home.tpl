{include file="header.tpl" title="Fondos AFP - Informaci&oacute;n gratuita y actualizada de fondos de pensi&oacute;n de Chile"}

<div class="padding">	
	<div class="indicador" id="capital">
		<h2><a href="http://{$smarty.server.SERVER_NAME}/fondos/afp-capital/" title="Rentabilidad de los fondos de la AFP Capital">AFP Capital</a></h2>
		{$tabla_capital}
	</div>
	<div class="indicador" id="cuprum">
		<h2><a href="http://{$smarty.server.SERVER_NAME}/fondos/afp-cuprum/" title="Rentabilidad de los fondos de la AFP Cuprum">AFP Cuprum</a></h2>
		{$tabla_cuprum}
	</div>
	<div class="clear"></div>
	<div class="indicador" id="habitat">
		<h2><a href="http://{$smarty.server.SERVER_NAME}/fondos/afp-habitat/" title="Rentabilidad de los fondos de la AFP Habitat">AFP Habitat</a></h2>
		{$tabla_habitat}
	</div>
	<div class="indicador" id="modelo">
		<h2><a href="http://{$smarty.server.SERVER_NAME}/fondos/afp-modelo/" title="Rentabilidad de los fondos de la AFP Modelo">AFP Modelo</a></h2>
		{$tabla_modelo}
	</div>	
	<div class="clear"></div>
	<div class="indicador" id="planvital">
		<h2><a href="http://{$smarty.server.SERVER_NAME}/fondos/afp-planvital/" title="Rentabilidad de los fondos de la AFP Planvital">AFP PlanVital</a></h2>
		{$tabla_planvital}
	</div>
	<div class="indicador" id="provida">
		<h2><a href="http://{$smarty.server.SERVER_NAME}/fondos/afp-provida/" title="Rentabilidad de los fondos de la AFP Provida">AFP Provida</a></h2>
		{$tabla_provida}
	</div>	
	<div class="clear"></div>
	<div class="fb-comments fb-comments-home" data-href="https://www.fondosafp.com/" data-num-posts="5" data-width="850"></div>
</div>
{if $smarty.server.SERVER_NAME == 'www.fondosafp.com' }
<img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6009878898351&amp;value=0" />
{/if}
{include file="footer.tpl"}
