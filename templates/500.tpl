{include file="header.tpl" title="Fondos AFP - Error Interno"}

<div class="padding">
	<h2>Error interno!</h2>
	<p>Lo sentimos, ha ocurrido un error interno.</p>
	<p>No te preocupes, ya estamos trabajando en él.</p>
	<div class="clear"></div>
</div>
{include file="footer.tpl"}