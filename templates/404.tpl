{include file="header.tpl" title="FondosAFP - Recurso no encontrado!"}

<div class="padding">
	<h2>Recurso no encontrado!</h2>
	<p>El recurso <b>{$smarty.server.REQUEST_URI}</b> no ha sido encontrado en este servidor</p>
	<div class="clear"></div>
</div>
{include file="footer.tpl"}