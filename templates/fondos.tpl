{include file="header.tpl" title=$title}

<div class="padding">
	<div class="fondos_options">
		<div id="top_filters">
			{html_options name=afp id=afp options=$afps selected=$defaultAFP selected="{$afp_api_name}"}
			<input type="text" placeholder="Desde" name="desde" id="desde" value="{$desde}" readonly="readonly"/>
			&rarr;
			<input type="text" placeholder="Hasta" name="hasta" id="hasta" value="{$hasta}" readonly="readonly" />
		</div>
		<div id="bottom_filters">
			{foreach $fondos key=fid item=fondo name="fondofor"}
			<input type="checkbox" name="fondos[]" class="data_fondos" value="{$fid}" {if in_array($fid, $fondos_ids)}checked="checked"{/if} id="fondo_{$smarty.foreach.fondofor.iteration}" />
			<label for="fondo_{$smarty.foreach.fondofor.iteration}">Fondo {$fondo}</label>
			{/foreach}
		</div>
		<div id="aditional_filters">
			<a>[+] mas opciones</a>
			<div>
				<input type="checkbox" name="ocultarFDS" id="ocultarFDS" value="1" {if $ocultarFDS} checked="checked"{/if} />
				<label for="ocultarFDS">Ocultar fines de semana</label>
				|
				<select id="sel_tipo" name="tipo">
					<option value="valor" {if $tipo=='valor'}selected="selected"{/if}>Mostrar valor</option>
					<option value="porcentaje" {if $tipo=='porcentaje'}selected="selected"{/if}>Mostrar porcentaje</option>
					{if true}<option value="patrimonio" {if $tipo=='patrimonio'}selected="selected"{/if}>Mostrar patrimonio</option>{/if}
				</select>
			</div>
		</div>
		<input type="submit" name="submit" value="Mostrar información de mis fondos" onclick="return updateChart();"/>
	</div>
	<div id="chart_container"> </div>
	<div class="details">
		<div class="box-rentabilidad-header">
			<label class="bold" id="afp_name"> </label>
			<label id="periodo"> </label>
		</div>
	</div>
	<div class="clear"></div>
	<div class="fb-comments fb-comments-fondos" data-href="https://www.fondosafp.com{$smarty.server.REQUEST_URI}" data-num-posts="5" data-width="850"></div>
</div>
{if $smarty.server.SERVER_NAME == 'www.fondosafp.com' }
<img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6009878828351&amp;value=0" />
{/if}
{include file="footer.tpl"}