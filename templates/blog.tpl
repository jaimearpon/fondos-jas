{include file="header.tpl" title="{if isset($blog.single)}{$blog.posts.0.title}{else}Blog{/if} - Fondos AFP{if isset($blog.page) && $blog.page > 1}, p&aacute;gina {$blog.page} de {$blog.info.pagecount}{/if}"}
<div class="padding blog">
	{foreach $blog.posts key=id item=post}
	<div class="post">
		<div class="date-info">
			<div class="date"><p>{$post.day}<span>{$post.month} <label>{$post.year}</label></span></p></div>
		</div>
		{if isset($blog.single)}
		<h2>{$post.title}</h2>
		{else}
		<h2><a href="{$post.permalink}">{$post.title}</a></h2>
		{/if}
		<div class="post-content">{$post.body}</div>
		<div class="social_actions">
			<div class="facebook_like_box"><div class="fb-like" data-href="{$post.permalink}" data-send="true" data-layout="button_count" data-width="200" data-show-faces="false"></div></div>
			<div class="twitter_count_box"><a href="https://twitter.com/share" class="twitter-share-button" data-url="{$post.permalink}" data-text="{$post.title}" data-via="FondosAFP" data-lang="es" data-related="FondosAFP" data-hashtags="FondosAFP">Twittear</a></div>
			<div class="google_plus_box"><div class="g-plusone" data-size="medium" data-annotation="inline" data-width="120" data-href="{$post.permalink}"></div></div>
			{if !isset($blog.single)}
			<div class="comments_count">
				<a href="{$post.permalink}#comments"><fb:comments-count href="{$post.permalink}">cargando</fb:comments-count> comentarios</a>
			</div>
			{/if}
		</div>
		{if isset($blog.single)}
		<a name="comments"></a>
		<div class="comments">
			<div class="fb-comments" data-href="{$post.permalink}" data-width="800" data-num-posts="10"></div>
		</div>
		{/if}
	</div>
	<div class="clear"></div>
	{/foreach}

	{if !isset($blog.single)}
	<div class="pagination">
		<p>Página {$blog.page} de {$blog.info.pagecount}</p>
		{if $blog.page > 1}
			{if $blog.page == 2}
				<a href="{$smarty.const.SERVER_URL}blog/" class="page back">← Volver</a>
			{else}
				<a href="{$smarty.const.SERVER_URL}blog/page/{$blog.page-1}/" class="page back">← Volver</a>
			{/if}
		{/if}
		{if $blog.page < $blog.info.pagecount}
		<a href="{$smarty.const.SERVER_URL}blog/page/{$blog.page+1}/" class="page more">M&aacute;s →</a>
		{/if}
	</div>
	{/if}
	<div class="clear"></div>
</div>
{if $smarty.server.SERVER_NAME == 'www.fondosafp.com' }
<img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6009878911351&amp;value=0" />
{/if}
{include file="footer.tpl"}