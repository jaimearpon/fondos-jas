{include file="header.tpl" title="Alertas - Fondos AFP"}
<div class="padding alerta">
	<h2>Alertas</h2>
	<div class="main-content">
	<script type="text/javascript">
		var _u = {$ultimos_valores};
		function ultimoValor(){
			$('#uv').text(_u[$('#afp').val()][$('#fondo').val()]);
			mayor_menor();
		}
		function mayor_menor() {
			if ($('#tipo').val() == "0"){
				$('#valor').val(Math.ceil(parseFloat(_u[$('#afp').val()][$('#fondo').val()])/10)*10);
			} else {
				$('#valor').val(Math.floor(parseFloat(_u[$('#afp').val()][$('#fondo').val()])/10)*10);
			}
		}
		$(document).ready(function(){
			ultimoValor();
			$('#tipo').bind('change', function(){
				mayor_menor();
			});
		});
	</script>
	<p>¿Necesitas enterarte del cambio del valor cuota de tu fondo?, crea una alerta y te avisaremos por correo si ocurre algún cambio.</p>
		<form id="form_alertas" class="contact" action="/alertas/" method="post">
			<fieldset>
				<label>&nbsp;</label>
				{html_options name=afp id="afp" options=$afps selected=$defaultAFP selected="{$afp_id}"  onchange="ultimoValor();"}
				<div class="clear"></div>
				
				<label>&nbsp;</label>
				{html_options name=fondo id="fondo" options=$fondos selected=$defaultFondo selected="{$fondo_id}"  onchange="ultimoValor();"}
				<div class="clear"></div>
				
				<label>Valor</label>
				<select name="tipo" id="tipo" class="element">
					<option value="0">mayor o igual que</option>
					<option value="1">menor o igual que</option>
				</select>
				<input type="text" name="valor" placeholder="" id="valor" class="element" maxlength="5" value="" /> <span id="uvvm">(&uacute;ltimo valor: $<span id="uv">&nbsp;</span>)</span>
				<div class="clear"></div>
				
				<label>Tu email</label>
				<input type="text" name="email" id="email" class="element" value="" />
				<div class="clear"></div>
				<input type="submit" name="submit" id="btn_crear_alerta" class="button" value="Crear alerta" onclick="return crearAlerta();" />
			</fieldset>
		</form>
	</div>
	<div class="side-content">
		<div class="page-flip">
			<h4 class="box-title">Recuerda!</h4>
			<em>Puedes crear cuantas alertas quieras, las veces que quieras.</em>
		</div>
	</div>
	<div class="clear"></div>
	<div class="fb-comments fb-comments-alertas" data-href="https://www.fondosafp.com/alertas/" data-num-posts="5" data-width="850"></div>
</div>
{if $smarty.server.SERVER_NAME == 'www.fondosafp.com' }
<img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6009878892551&amp;value=0" />
{/if}
{include file="footer.tpl"}
