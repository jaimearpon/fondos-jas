-- Function: variacionporcentual_api(text, text, integer, integer)

-- DROP FUNCTION variacionporcentual_api(text, text, integer, integer);

CREATE OR REPLACE FUNCTION variacionporcentual_api(text, text, integer, integer)
  RETURNS SETOF variacion_api AS
$BODY$
declare
r variacion_api%rowtype;
v numeric;
begin

select valor into v from cuotas where afp_id = $3 and fondo_id = $4 and fecha >= $1 and fecha <= $2;

for r in SELECT cuotas.fondo_id, cuotas.valor, cuotas.fecha, fondo.api_name FROM cuotas INNER JOIN fondo ON (cuotas.fondo_id = fondo.fondo_id) WHERE cuotas.fecha >= $1 AND cuotas.fecha <= $2 AND cuotas.afp_id = $3 AND cuotas.fondo_id = $4 ORDER BY cuotas.fecha ASC loop
  r.valor = round((r.valor*100)/v - 100,2);
  return next r;
end loop;
return;
end
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION variacionporcentual_api(text, text, integer, integer)
  OWNER TO afp_user;
