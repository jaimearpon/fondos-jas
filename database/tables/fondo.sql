-- Table: fondo

-- DROP TABLE fondo;

CREATE TABLE fondo
(
  fondo_id serial NOT NULL,
  nombre character(1) NOT NULL,
  descripcion character varying(128),
  api_name character(1),
  country character(20),
  status numeric(1,0), -- 1: está activa y operando, 0: no está activa u operando
  CONSTRAINT pk_fondo PRIMARY KEY (fondo_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE fondo
  OWNER TO afp_user;
COMMENT ON COLUMN fondo.status IS '1: está activa y operando, 0: no está activa u operando';

