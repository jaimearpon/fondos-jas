-- Table: email_queue

-- DROP TABLE email_queue;

CREATE TABLE email_queue
(
  id serial NOT NULL,
  created timestamp without time zone NOT NULL DEFAULT now(),
  status character(12) NOT NULL DEFAULT 'none'::bpchar,
  sent timestamp without time zone,
  de character(250) NOT NULL,
  para character(250) NOT NULL,
  subject character(250) NOT NULL,
  body text NOT NULL,
  CONSTRAINT email_queue_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE email_queue
  OWNER TO afp_user;
