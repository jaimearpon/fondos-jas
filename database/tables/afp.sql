-- Table: afp

-- DROP TABLE afp;

CREATE TABLE afp
(
  afp_id serial NOT NULL,
  nombre character varying(30) NOT NULL,
  descripcion character varying(128),
  api_name character(20),
  country character(3), -- Codigo del pais, ejemplo CL = Chile, ARG = Argentina
  status numeric(1,0), -- 1 = Esta operando, 0: no está operando, 2: Otro
  CONSTRAINT pk_afp PRIMARY KEY (afp_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE afp
  OWNER TO afp_user;
COMMENT ON COLUMN afp.country IS 'Codigo del pais, ejemplo CL = Chile, ARG = Argentina';
COMMENT ON COLUMN afp.status IS '1 = Esta operando, 0: no está operando, 2: Otro';

