-- Table: alerta

-- DROP TABLE alerta;

CREATE TABLE alerta
(
  id serial NOT NULL,
  usuario_id integer NOT NULL,
  afp_id integer NOT NULL,
  fondo_id integer NOT NULL,
  tipo smallint NOT NULL,
  valor real NOT NULL,
  created timestamp without time zone NOT NULL DEFAULT now(),
  modified timestamp without time zone NOT NULL DEFAULT now(),
  enviada boolean NOT NULL DEFAULT false,
  CONSTRAINT alert_pk PRIMARY KEY (id),
  CONSTRAINT alerta_fk_afp_id FOREIGN KEY (afp_id)
      REFERENCES afp (afp_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT alerta_fk_fondo_id FOREIGN KEY (fondo_id)
      REFERENCES fondo (fondo_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT alerta_fk_usuario_id FOREIGN KEY (usuario_id)
      REFERENCES usuario (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE alerta
  OWNER TO afp_user;
