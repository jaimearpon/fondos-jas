-- Table: cuotas

-- DROP TABLE cuotas;

CREATE TABLE cuotas
(
  cuota_id serial NOT NULL,
  fecha character varying(10) NOT NULL,
  afp_id bigint NOT NULL,
  fondo_id bigint NOT NULL,
  valor double precision NOT NULL,
  patrimonio double precision,
  CONSTRAINT pk_cuotas PRIMARY KEY (cuota_id),
  CONSTRAINT fk_afp FOREIGN KEY (afp_id)
      REFERENCES afp (afp_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_fondo FOREIGN KEY (fondo_id)
      REFERENCES fondo (fondo_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_reg UNIQUE (afp_id, fondo_id, fecha)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cuotas
  OWNER TO afp_user;
