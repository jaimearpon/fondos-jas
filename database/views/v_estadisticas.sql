-- View: v_estadisticas

-- DROP VIEW v_estadisticas;

CREATE OR REPLACE VIEW v_estadisticas AS 
 SELECT DISTINCT ( SELECT count(1) AS count
           FROM usuario) AS usuarios, ( SELECT count(DISTINCT alerta.usuario_id) AS count
           FROM alerta) AS usuarios_activos, ( SELECT count(DISTINCT alerta.usuario_id) AS count
           FROM alerta
          WHERE alerta.created < (now() - '7 days'::interval)) AS activos_7_dias, ( SELECT count(DISTINCT alerta.usuario_id) AS count
           FROM alerta
          WHERE alerta.created < (now() - '31 days'::interval)) AS activos_31_dias, ( SELECT count(DISTINCT alerta.usuario_id) AS count
           FROM alerta
          WHERE alerta.created < (now() - '3 mons'::interval)) AS activos_3_meses, ( SELECT count(DISTINCT alerta.usuario_id) AS count
           FROM alerta
          WHERE alerta.created < (now() - '6 mons'::interval)) AS activos_6_meses, ( SELECT count(DISTINCT alerta.usuario_id) AS count
           FROM alerta
          WHERE alerta.created < (now() - '1 year'::interval)) AS activos_12_meses, ( SELECT count(1) AS count
           FROM v_alertas) AS alertas, ( SELECT count(1) AS count
           FROM v_alertas
          WHERE v_alertas.estado = 'Enviada'::text) AS alertas_enviadas, ( SELECT count(1) AS count
           FROM v_alertas
          WHERE v_alertas.estado = 'Pendiente'::text) AS alertas_pendientes, ( SELECT count(1) AS count
           FROM alerta
          WHERE alerta.created > (now() - '24:00:00'::interval)) AS alertas_ultimas_24_horas, ( SELECT count(1) AS count
           FROM alerta
          WHERE alerta.created > (now() - '7 days'::interval)) AS alertas_ultimas_7_dias
   FROM v_alertas;

ALTER TABLE v_estadisticas
  OWNER TO afp_user;

