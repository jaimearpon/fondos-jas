-- View: v_alertas

-- DROP VIEW v_alertas;

CREATE OR REPLACE VIEW v_alertas AS 
 SELECT usuario.created AS fecha_alta, usuario.email, alerta.created AS ultima_alerta, afp.nombre AS afp, fondo.nombre AS fondo, 
        CASE
            WHEN alerta.tipo = 0 THEN 'mayor'::text
            ELSE 'menor'::text
        END AS tipo, alerta.valor, 
        CASE
            WHEN alerta.enviada = true THEN 'Enviada'::text
            ELSE 'Pendiente'::text
        END AS estado
   FROM usuario usuario
   JOIN alerta alerta ON usuario.id = alerta.usuario_id
   JOIN afp afp ON afp.afp_id = alerta.afp_id
   JOIN fondo fondo ON fondo.fondo_id = alerta.fondo_id
  WHERE lower(usuario.email::text) <> 'julioarayacerda@gmail.com'::text
  ORDER BY alerta.created DESC;

ALTER TABLE v_alertas
  OWNER TO afp_user;

