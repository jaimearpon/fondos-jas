-- View: v_cuotas

-- DROP VIEW v_cuotas;

CREATE OR REPLACE VIEW v_cuotas AS 
 SELECT c.cuota_id, c.fecha, a.nombre AS afp, f.nombre AS fondo, c.valor
   FROM afp a, cuotas c, fondo f
  WHERE c.afp_id = a.afp_id AND c.fondo_id = f.fondo_id;

ALTER TABLE v_cuotas
  OWNER TO afp_user;

