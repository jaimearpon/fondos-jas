-- View: v_porcentajemensual

-- DROP VIEW v_porcentajemensual;

CREATE OR REPLACE VIEW v_porcentajemensual AS 
 SELECT 100::double precision * (a.valor / (( SELECT b.valor
           FROM cuotas b
          WHERE b.afp_id = a.afp_id AND b.fondo_id = a.fondo_id AND to_date(b.fecha::text, 'yyyy-mm-dd'::text) = date_trunc('month'::text, to_date(a.fecha::text, 'yyyy-mm-dd'::text)::timestamp with time zone)))) AS valor, a.afp_id, a.fondo_id, a.fecha
   FROM cuotas a;

ALTER TABLE v_porcentajemensual
  OWNER TO afp_user;

