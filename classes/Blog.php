<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Blog
 *
 * @author Julio Araya
 */
final class Blog {

	private static $_months = array(
		1  => 'ENE',
		2  => 'FEB',
		3  => 'MAR',
		4  => 'ABR',
		5  => 'MAY',
		6  => 'JUN',
		7  => 'JUL',
		8  => 'AGO',
		9  => 'SEP',
		10 => 'OCT',
		11 => 'NOV',
		12 => 'DIC',
	);

	const CACHE_TIMEOUT = 3600;
	const PAGE_LIMIT = 5;
	/**
	 * Obtiene los ultimos posts desde Tumblr
	 *
	 * @param int $page El número de página 0 = primera, 1: segunda, etc
	 * @return array Un arreglo con los posts de Tumblr
	 */
	public static function getPosts ($page = 1) {
		if ((int)$page < 1) {
			$page = 1;
		}
		
		$limit = self::PAGE_LIMIT;
		$offset = ($page - 1) * $limit;
		$apc_key = $_SERVER['SERVER_NAME'] . "_posts_page_$page"."_".$limit;
		
		if (function_exists('apc_fetch')) {
			$apc_val = apc_fetch($apc_key);
			if ( $apc_val !== false ) {
				Log::getInstance()->log("[blog] Retornando posts de la caché $apc_key");
				return $apc_val;
			}
		}

		Log::getInstance()->log("[blog] Tumblr API request, page $page");
		$api_key = 'cBBa7XK1Spw4OMPNRhL2zuh5n1YV617Ka1fZANiWrXEe8w6rKK';
		$api_url = "http://api.tumblr.com/v2/blog/fondosafp.tumblr.com/posts?type=text&limit=$limit&offset=$offset&api_key=$api_key";
		$ch = curl_init($api_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = json_decode(curl_exec($ch), true);
		curl_close($ch);
		$posts = array();
		if ($response['meta']['status'] == 200 && $response['meta']['msg'] == 'OK') {
			$posts = $response['response']['posts'];
			foreach ($posts as $k => $post) {
				$posts[$k] = self::_cleanPost($post);
			}
			if (function_exists('apc_store')) {
				Log::getInstance()->log("[blog] Guardando posts $apc_key");
				apc_store($apc_key, $posts, self::CACHE_TIMEOUT);
			}
		} else {
			$amazonSES = AmazonSES::getInstance();
			$amazonSES->sendEmail('julio.araya@fondosafp.cl', AWS_EMAIL_CONTACTO, 'Tumblr API error', print_r($response, true));
		}
		return $posts;
	}

	/**
	 * Obtiene la información de un post por su ID
	 *
	 * @param int $id Id del post
	 * @return array
	 */
	public static function getPost ($id) {
		$apc_key = $_SERVER['SERVER_NAME'] . "_post_$id";

		if (function_exists('apc_fetch')) {
			$apc_val = apc_fetch($apc_key);
			if ( $apc_val !== false ) {
				Log::getInstance()->log("[blog] Retornando post $id de la caché $apc_key");
				return $apc_val;
			}
		}

		Log::getInstance()->log("[blog] Tumblr API request, post $id");
		$api_key = 'cBBa7XK1Spw4OMPNRhL2zuh5n1YV617Ka1fZANiWrXEe8w6rKK';
		$api_url = "http://api.tumblr.com/v2/blog/fondosafp.tumblr.com/posts?type=text&id=$id&api_key=$api_key";
		$ch = curl_init($api_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = json_decode(curl_exec($ch), true);
		curl_close($ch);
		$posts = array();
		if ($response['meta']['status'] == 200 && $response['meta']['msg'] == 'OK') {
			$posts = $response['response']['posts'];
			foreach ($posts as $k => $post) {
				$posts[$k] = self::_cleanPost($post);
			}
			if (function_exists('apc_store')) {
				Log::getInstance()->log("[blog] Guardando posts $apc_key");
				apc_store($apc_key, $posts, self::CACHE_TIMEOUT);
			}
		} else {
			$amazonSES = AmazonSES::getInstance();
			$amazonSES->sendEmail('julio.araya@fondosafp.cl', AWS_EMAIL_CONTACTO, 'Tumblr API error', print_r($response, true));
		}
		return $posts;
	}

	public static function getBlogInfo() {
		$apc_key = $_SERVER['SERVER_NAME'] . "_blog_info";

		if (function_exists('apc_fetch')) {
			$apc_val = apc_fetch($apc_key);
			if ( $apc_val !== false ) {
				Log::getInstance()->log("[blog] Retornando blog info de la caché $apc_key");
				return $apc_val;
			}
		}

		Log::getInstance()->log("[blog] Tumblr API request, blog info");
		$api_key = 'cBBa7XK1Spw4OMPNRhL2zuh5n1YV617Ka1fZANiWrXEe8w6rKK';
		$api_url = "http://api.tumblr.com/v2/blog/fondosafp.tumblr.com/info?api_key=$api_key";
		$ch = curl_init($api_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = json_decode(curl_exec($ch), true);
		curl_close($ch);
		$blog = array();
		if ($response['meta']['status'] == 200 && $response['meta']['msg'] == 'OK') {
			$blog = $response['response']['blog'];
			$blog['pagecount'] = floor($blog['posts']/self::PAGE_LIMIT);
			if (function_exists('apc_store')) {
				Log::getInstance()->log("[blog] Guardando posts $apc_key");
				apc_store($apc_key, $blog, self::CACHE_TIMEOUT);
			}
		} else {
			$amazonSES = AmazonSES::getInstance();
			$amazonSES->sendEmail('julio.araya@fondosafp.cl', AWS_EMAIL_CONTACTO, 'Tumblr API error', print_r($response, true));
		}
		return $blog;
	}

	/**
	 * Realiza una búsqueda en el título y el contenido de los posts y retorna
	 * los posts que coincidan con el termino de búsqueda
	 *
	 * @param string $term Término de búsqueda
	 * @return array Los posts que coincidan con la busqueda
	 */
	public static function search ($term) {
		
	}

	/**
	 * Limpia y formatea el post de tumblr
	 *
	 * @param array $post Post de Tumblr
	 * @return array El post formateado
	 */
	private static function _cleanPost ($post) {
		unset($post['blog_name']);
		unset($post['date']);
		unset($post['state']);
		unset($post['format']);
		unset($post['short_url']);
		unset($post['highlighted']);
		unset($post['source_url']);
		unset($post['source_title']);
		$post['day'] = date("d", $post['timestamp']);
		$post['month'] = self::_monthName((int)date("m", $post['timestamp']));
		$post['year'] = date("Y", $post['timestamp']);
		$post['body'] = str_replace('target="_blank"', '', $post['body']);
		$post['body'] = str_replace('http://www.fondosafp.cl/', SERVER_URL, $post['body']);
		$post['permalink'] = SERVER_URL."blog/{$post['id']}/{$post['slug']}/";
		return $post;
	}

	/**
	 * Devuelve el nombre corto del mes
	 *
	 * @param int $monthNumber
	 * @return string
	 */
	private static function _monthName ($monthNumber) {
		if (array_key_exists($monthNumber, self::$_months)) {
			return self::$_months[$monthNumber];
		}
		return '';
	}

}
