<?php

/**
 * Description of AmazonSES
 *
 * see http://www.orderingdisorder.com/aws/ses/
 * @author jaraya
 */

define('AWS_SECRET_KEY_ID', '0HJ7XHXFCCE4JW4V23G2');
define('AWS_SECRET_KEY','gEFvAoEP0jcu2RhJEUcTUYiSs1lUR4VbbmshU9+E');


final class AmazonSES {
	
	const TABLE_QUEUE = 'email_queue';
	
	const EMAIL_STATUS_SENT		= 'sent';
	const EMAIL_STATUS_SENDING	= 'sending';
	const EMAIL_STATUS_NONE		= 'none';
	const EMAIL_STATUS_DELETED	= 'deleted';
	const EMAIL_STATUS_BLACKLISTED	= 'blacklisted';
	const EMAIL_STATUS_FAIL		= 'fail';


	private $_from;
	private $_lastMD5 = null;
	
	/**
	 *
	 * @var SimpleEmailService 
	 */
	private $ses;
	
	/**
	 * The object instance
	 *
	 * @var AmazonSES
	 */
	private static $_instance = null;

	private function  __construct() {
		$this->_from = AWS_EMAIL_ALERT;
		$this->ses = new SimpleEmailService(AWS_SECRET_KEY_ID, AWS_SECRET_KEY);
	}

	/**
	 * Get the instance (singleton)
	 *
	 * @return AmazonSES The instance of this class
	 */
	public static function getInstance(){
		if ( !isset(self::$_instance) || self::$_instance == null ) {
			$c = __CLASS__;
			self::$_instance = new $c;
		}
		return self::$_instance;
	}

	
	/**
	 * Send an email
	 *
	 * @param string|array $to
	 * @param string $subject
	 * @param string $body
	 * @return bool True if the email was sent 
	 */
	public function sendEmail($para, $from, $subject, $body, $enqueue = true) {
		if( is_array($para) )
			throw new Exception(__FILE__);
		
		$md5 = md5($para."::".$subject."::".$body);
		if ( $md5 == $this->_lastMD5 ) {
			Log::getInstance()->log("[amazon] Enviando el mismo correo 2 veces... omitiendo");
			return true;
		}
		$this->_lastMD5 = $md5;

		if( $enqueue ) {
			self::enqueueEmail( $para, $from, $subject, $body );
			return true;
		}

		Log::getInstance()->log("Enviando correo a '$para'");			

		usleep(1000000);//only 1 email per second
		$m = new SimpleEmailServiceMessage();
		$m->addTo($para);
		$m->setFrom($from);
		$m->setSubject($subject);
		$m->setMessageFromString(null, $body);
		$this->ses->enableVerifyHost(false);
		$this->ses->enableVerifyPeer(false);
		$response = $this->ses->sendEmail($m);
		
		//Log::getInstance()->log("[amazon] Response: " . print_r($response, true));
		
		//if ( strpos($response, 'Address blacklisted.') )
		//	throw new EmailBlacklistedException("La dirección '$para' está en lista negra");

		$ok = isset($response['MessageId']);
		if( !$ok )
			Log::getInstance()->log("No se ha podido enviar el siguiente mensaje: '$subject' a '$para'. Trace: $response");

		return $ok;
	}
	
	/**
	 * Enqueque an email for delayed delivering
	 *
	 * @param string $to The email
	 * @param string $subject 
	 * @param string $body 
	 */
	private static function enqueueEmail ($to, $from, $subject, $body ) {
		$rows_affected = 0;
		try {
			$sql  = ' INSERT INTO ' . self::TABLE_QUEUE . ' (para, de, subject, body, status) ';
			$sql .= ' VALUES (:para, :de, :subject, :body, :status) ';
			
			$params = array(
				':para'		=> $to,
				':de'		=> $from,
				':subject'	=> $subject,
				':body'		=> $body,
				':status'	=> self::EMAIL_STATUS_NONE
			);
			$id = Database::getInstance()->execute($sql, $params, true);
			Log::getInstance()->log("[AmazonSES] email enqueued, id: $id");
			$rows_affected = $id ? 1 : 0;
		} catch (Exception $ex) {
			Log::getInstance()->logException($ex);
		}
		return ( $rows_affected == 1 );
	}
	
	/**
	 * Sent the enqueued emails 
	 *
	 * @return int Number of emails sent
	 */
	public static function sendEmails () {
		$max_emails_to_send = 60;
		$sent = 0;
		try
		{
			$sql = 'SELECT * FROM ' . self::TABLE_QUEUE . ' WHERE status = :status_none LIMIT ' . intval($max_emails_to_send) . ' ';
			$params = array(':status_none' => self::EMAIL_STATUS_NONE);
			$rows = Database::getInstance()->query($sql, $params);
			
			if ( !$rows || count($rows) <= 0 ) {
				return 0;
			}

			//marcar los campos como enviados
			$msg_ids = array();
			foreach( $rows as $row ) $msg_ids[] = intval($row['id']);

			$sql = ' UPDATE ' . self::TABLE_QUEUE . ' SET status = :sending_status WHERE id IN ( ' . implode( ',', $msg_ids ) . ' )';
			$params = array(':sending_status' => self::EMAIL_STATUS_SENDING);
			Database::getInstance()->execute($sql, $params);

			//send the emails
			foreach ( $rows as $row ) {
				try {
					//send the email
					$id = $row['id'];
					$para = $row['para'];
					$from = $row['de'];
					$subject = $row['subject'];
					$body = $row['body'];
					$wasSent = self::getInstance()->sendEmail($para, $from, $subject, $body, false);
					if ( $wasSent ) {
						self::_setEmailStatus($id, self::EMAIL_STATUS_SENT);
						$sent++;
					} else {
						self::_setEmailStatus($id, self::EMAIL_STATUS_FAIL);
					}
				} catch ( EmailBlacklistedException $eblex) {
					Log::getInstance()->logException($eblex);
					self::_setEmailStatus($id, self::EMAIL_STATUS_BLACKLISTED);
				} catch ( Exception $emailEx ) {
					Log::getInstance()->logException($emailEx);
					self::_setEmailStatus($id, self::EMAIL_STATUS_FAIL);
				}
			}
		} catch ( Exception $ex ) {
			Log::getInstance()->logException($ex);
		}

		return $sent;
	}
	
	
	
	private static function _setEmailStatus ( $id , $status ) {
		try {
			$sql = 'UPDATE ' . self::TABLE_QUEUE . ' SET status = :status, sent = CURRENT_TIMESTAMP  WHERE id = :id';
			$params = array( ':id' => $id, ':status' => $status );
			Database::getInstance()->execute($sql, $params);
		} catch ( Exception $ex ) {
			Log::getInstance()->logException($ex);
		}
	}
	
	
	
}
