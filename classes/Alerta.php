<?php

/**
 * Description of Alerta
 *
 * @author delpho
 */
final class Alerta extends FondoObject {
	
	const TABLE = 'alerta';
	
	const TIPO_MAYOR_O_IGUAL_QUE = 0;
	const TIPO_MENOR_O_IGUAL_QUE = 1;
	
	private $_usuario_id;
	private $_afp_id;
	private $_fondo_id;
	private $_tipo;
	private $_valor;
	private $_enviada;
	
	/**
	 * Retorna el identificador del usuario
	 * 
	 * @return int
	 */
	public function getUsuarioID () {
		return $this->_usuario_id;
	}
	
	/**
	 * Establece el identificador del usuario
	 * 
	 * @param int $usuario_id
	 * @return Alerta 
	 */
	public function setUsuarioID ($usuario_id) {
		$this->_usuario_id = Utils::forzarEnteroPositivo($usuario_id);
		return $this;
	}
	
	/**
	 * Retorna el identificador de la AFP
	 * 
	 * @return int
	 */
	public function getAfpID () {
		return $this->_afp_id;
	}
	
	/**
	 * Establece el identificador de la AFP
	 * 
	 * @param int $afp_id
	 * @return Alerta 
	 */
	public function setAfpID ( $afp_id ) {
		$this->_afp_id = Utils::forzarEnteroPositivo($afp_id);
		return $this;
	}
	
	/**
	 * Retorna el identificador del fondo
	 * 
	 * @return int
	 */
	public function getFondoID () {
		return $this->_fondo_id;
	}
	
	/**
	 * Establece el identificador del fondo
	 * 
	 * @param int $fondo_id
	 * @return Alerta 
	 */
	public function setFondoID ( $fondo_id ) {
		$this->_fondo_id = Utils::forzarEnteroPositivo($fondo_id);
		return $this;
	}
	
	/**
	 * Retorna el tipo de la alerta
	 * 
	 * @return int
	 */
	public function getTipo () {
		return $this->_tipo;
	}
	
	/**
	 * Establece el tipo de la alerta
	 * 
	 * @param type $tipo 
	 * @return Alerta
	 */
	public function setTipo ( $tipo ) {
		$this->_tipo = Utils::forzarValorEnum($tipo, array(
		    self::TIPO_MAYOR_O_IGUAL_QUE, 
		    self::TIPO_MENOR_O_IGUAL_QUE
		), 'El tipo de alerta no es válido');
		return $this;
	}
	
	/**
	 * Retorna el valor de la alerta
	 * 
	 * @return float
	 */
	public function getValor () {
		return $this->_valor;
	}
	
	/**
	 * Establece el valor de la alerta
	 * 
	 * @param int $valor
	 * @return Alerta 
	 */
	public function setValor ( $valor ) {
		$this->_valor = Utils::forzarDecimalPositivo($valor);
		return $this;
	}
	
	/**
	 * Ha sido enviada esta alerta?
	 * 
	 * @return bool
	 */
	public function isEnviada () {
		return (bool)$this->_enviada;
	}
	
	/**
	 * Establece si esta alerta ha sido enviada o no.
	 * 
	 * @param bool $enviada
	 * @return Alerta 
	 */
	public function setEnviada( $enviada ) {
		$this->_enviada = (bool) $enviada;
		return $this;
	}
	
	
	public function save () {
		//chequear los valores 
		if ( $this->getUsuarioID() === null )
			throw new Exception(__LINE__);
		elseif ( $this->getAfpID() === null)
			throw new Exception(__LINE__);
		elseif ( $this->getFondoID() === null )
			throw new Exception(__LINE__);
		elseif ( $this->getTipo() === null )
			throw new Exception(__LINE__);
		elseif ( $this->getValor() === null) 
			throw new Exception(__LINE__);
		
		if ( !$this->getID() ) {
			$created = self::_alreadyCreated($this->getAfpID(), $this->getFondoID(), $this->getTipo(), $this->getValor(), $this->getUsuarioID());
			if ( $created ) {
				return null;
				//throw new LogicException('Esta alerta ya ha sido creada, por favor selecciona otras opciones');
			}
			
			$sql  = ' INSERT INTO ' . self::TABLE ;
			$sql .= ' (usuario_id, afp_id, fondo_id, tipo, valor, enviada) ';
			$sql .= ' VALUES(:usuario_id, :afp_id, :fondo_id, :tipo, :valor, :enviada) ';
			$params = array(
				':usuario_id' => $this->getUsuarioID(),
				':afp_id' => $this->getAfpID(),
				':fondo_id' => $this->getFondoID(),
				':tipo' => $this->getTipo(),
				':valor' => $this->getValor(),
				':enviada' => (int)$this->isEnviada()
			);
			$this->_id = Database::getInstance()->execute($sql, $params, true);
		} else {
			
			$sql  = ' UPDATE ' . self::TABLE . ' SET ';
			$sql .= ' enviada = :enviada ';
			$sql .= ' modified = CURRENT_TIMESTAMP ';
			$sql .= ' WHERE id = :id ';
			$params = array(
				':id' => $this->getID(),
				':enviada' => (int)$this->isEnviada()
			);
			Database::getInstance()->execute($sql, $params);
		}
		return true;
	}
	
	
	
	/**
	 * Crea una alerta
	 *
	 * @param array $opciones
	 * @return Alerta 
	 */
	public static function crearAlerta ( array $opciones ) {
		$afp	= isset($opciones['afp'])   ? $opciones['afp']   : null;
		$fondo	= isset($opciones['fondo']) ? $opciones['fondo'] : null;
		$tipo	= isset($opciones['tipo'])  ? $opciones['tipo']  : null;
		$valor	= isset($opciones['valor']) ? $opciones['valor'] : null;
		$email	= $opciones['email'];
	
		$usuario = new Usuario($email);
		$usuario = new Usuario($email);
		$alerta = new Alerta();
		$alerta->setUsuarioID($usuario->getID());
		$alerta->setAfpID($afp);
		$alerta->setFondoID($fondo);
		$alerta->setTipo($tipo);
		$alerta->setValor($valor);
		$alerta->setEnviada(false);
		$creada = $alerta->save();
		
		return $creada ? $alerta : null;;
	}
	
	/**
	 * Verifica si la alerta ya a sido creada (y está activa)
	 *
	 * @param int $afp
	 * @param int $fondo
	 * @param int $tipo
	 * @param float $valor
	 * @param int $usuario_id
	 * @return boolean 
	 */
	private static function _alreadyCreated ( $afp, $fondo, $tipo, $valor, $usuario_id ) {
		$sql  = ' SELECT count(1) FROM ' .self::TABLE . ' WHERE ';
		$sql .= ' usuario_id = :usuario_id AND afp_id = :afp_id AND ';
		$sql .= ' fondo_id = :fondo_id AND tipo = :tipo AND ';
		$sql .= ' valor = :valor AND enviada = :enviada ';
		$params = array(
		    ':usuario_id' => $usuario_id,
		    ':afp_id' => $afp,
		    ':fondo_id' => $fondo,
		    ':tipo' => $tipo,
		    ':valor' => $valor,
		    ':enviada' => 0
		);
		$count = (int)Database::getInstance()->getOne($sql, $params);
		return $count > 0;
	}
	
	 
	/**
	 * Genera los emails de alertas y los agrega a la cola de emails
	 * 
	 * @return int Numero de email de alertas generados 
	 */
	public static function generarEmailDeAlertas () {
		$creados = 0;
		$sql = 'SELECT * FROM ' . self::TABLE . ' WHERE enviada = false ORDER BY afp_id ASC, fondo_id ASC, valor ASC ';
		$rows = Database::getInstance()->query($sql, array());
		$valores = Fondo::getValorFondos();
		foreach ( $rows as $row ) {
			$afp_id = $row['afp_id'];
			$fondo_id = $row['fondo_id'];
			$tipo = $row['tipo'];
			$valor = $row['valor'];
			$valor_actual = $valores[$afp_id][$fondo_id];
			if ( $tipo == self::TIPO_MAYOR_O_IGUAL_QUE && $valor_actual >= $valor ) {
				self::crearEmailAlerta($row, $valor_actual);
				$creados++;
			} elseif ( $tipo == self::TIPO_MENOR_O_IGUAL_QUE && $valor_actual <= $valor ) {
				self::crearEmailAlerta($row, $valor_actual);
				$creados++;
			}
		}
		return $creados;
	}
	
	private static function crearEmailAlerta($datos, $valor_actual) {
		$sql = 'UPDATE ' . self::TABLE . ' SET enviada = true WHERE id = :id ';
		$params = array(':id' => $datos['id']);
		Database::getInstance()->execute($sql, $params);
		$afps = array(
			1 => 'Capital',
			2 => 'Cuprum',
			3 => 'Habitat',
			4 => 'Modelo',
			5 => 'Plan Vital',
			6 => 'Provida'
		);
		$fondos = array(
			1 => 'A',
			2 => 'B',
			3 => 'C',
			4 => 'D',
			5 => 'E'
		);
		$subirbajar = $datos['tipo'] == self::TIPO_MAYOR_O_IGUAL_QUE ? 'superado los' : 'bajado de';
		$usuario = Usuario::getUsuarioById($datos['usuario_id']);
		$para = $usuario->getEmail();
		$notrack = $para == 'julioarayacerda@gmail.com' ? '&notrack' : '';
		$de = "\"Alertas FondosAFP\" <".AWS_EMAIL_ALERT.">";
		$subject = 'Alerta del valor de tus fondos';
		$content = "<p>Hola!</p>";
		$content .= "<p>Solo queremos avisarte que el valor ";
		$content .= "del <b>fondo {$fondos[$datos['fondo_id']]}</b> de ";
		$content .= "tu <b>AFP {$afps[$datos['afp_id']]}</b> ha ";
		$content .= "$subirbajar \${$datos['valor']}, el valor actual es \${$valor_actual}</p>";
		$content .= "<p>Puedes configurar mas alertas para el valor de tus fondos de pensi&oacute;n en ";
		$content .= "<a href='https://www.fondosafp.com/alertas/?ref=email_alerta$notrack'>https://www.fondosafp.com/alertas/</a></p>";
		$content .= "<p>--<br/>FondosAFP<br/><a href='https://www.fondosafp.com/?ref=email_alerta$notrack'>https://www.fondosafp.com/</a><br/>contacto@fondosafp.cl<br/>Santiago, Chile</p>";
		AmazonSES::getInstance()->sendEmail($para, $de, $subject, $content);
	}
	
	
}
