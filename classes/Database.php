<?php

/**
 * This class handle all database connections
 *
 * @author jaraya
 */
class Database {

	/**
	 * La instancia de la clase
	 *
	 * @var Database
	 */
	private static $_instance = null;

	/**
	 * La conexión a la base de datos
	 *
	 * @var resource
	 */
	private $_conn = null;

	/**
	 * The number of queries performed
	 *
	 * @var int
	 */
	private $_query_count = 0;

	/**
	 * Time elapsed in miliseconds
	 *
	 * @var int
	 */
	private $_time_elapsed = 0;
	
	
	private function  __construct() {}

	/**
	 * Obtiene la instancia de la clase (singleton)
	 *
	 * @return Database La instancia de esta clase
	 */
	public static function getInstance(){
		if ( !isset(self::$_instance) || self::$_instance == null ) {
			$c = __CLASS__;
			self::$_instance = new $c;
		}
		return self::$_instance;
	}

	/**
	 * Obtiene la conexión a la base de datos
	 *
	 * @return PDO
	 * @throws Exception
	 */
	private function getConnection(){
		if ( !$this->_conn || !($this->_conn instanceof PDO) ){
			$this->_conn =  new PDO('pgsql:host=fondosafp.cl;port=5432;dbname=fondos', 'afp_user', 'afpAFP123', array(  
						PDO::ATTR_PERSISTENT => false,
						PDO::ATTR_EMULATE_PREPARES => true,
						PDO::ATTR_TIMEOUT => 30 
					));
			if ( !($this->_conn instanceof PDO))
				throw new Exception('No se ha podido establecer una conexión a la base de datos');
		}
		return $this->_conn;
	}
	
	/**
	 * Ejecuta una consulta a la base de datos
	 * 
	 * @param string $sql
	 * @return array
	 */
	public function query($sql, $params) {
		$pdo = $this->getConnection();
		$stmt = $pdo->prepare($sql);
		$start = microtime(true);
		$this->_query_count++;
		$stmt->execute($params);
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		$this->_time_elapsed += microtime(true) - $start;
		return $rows;
	}
	
	/**
	 * Ejecuta una consulta a la base de datos y retorna la primera fila
	 * 
	 * @param string $sql
	 * @return array
	 */
	public function getRow($sql, $params) {
		$pdo = $this->getConnection();
		$stmt = $pdo->prepare($sql);
		$start = microtime(true);
		$this->_query_count++;
		$stmt->execute($params);
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
		$this->_time_elapsed += microtime(true) - $start;
		return $row;
	}
	
	/**
	 * Ejecuta una consulta a la base de datos que retorna un solo valor
	 * 
	 * @param string $sql
	 * @return mixed 
	 */
	public function getOne($sql, $params) {
		$pdo = $this->getConnection();
		$stmt = $pdo->prepare($sql);
		$start = microtime(true);
		$this->_query_count++;
		$stmt->execute($params);
		$value = null;
		$row = $stmt->fetch(PDO::FETCH_NUM);
		if ( $row )
			$value = $row[0];
		$stmt->closeCursor();
		$this->_time_elapsed += microtime(true) - $start;
		return $value;
	}
	
	/**
	 * Realiza una consulta de tipo INSERT/UPDATE en la base de datos
	 * 
	 * @param string $sql
	 * @params array $params
	 * @return int Numero de filas afectadas con la consulta
	 */
	public function execute ($sql, $params, $returnInsertedID = false) {
		$pdo = $this->getConnection();
		$stmt = $pdo->prepare($sql);
		foreach ( $params as $key => $value) {
			$stmt->bindValue($key, $value);
		}
		$start = microtime(true);
		$this->_query_count++;
		$stmt->execute();
		$affected_rows = $stmt->rowCount();
		$insertedID = null;
		if ( $affected_rows == 1 && $returnInsertedID ) {
			$insertedID = $pdo->lastInsertId();
		}
		$stmt->closeCursor();
		$this->_time_elapsed += microtime(true) - $start;
		if ( $returnInsertedID )
			return $insertedID;
		
		return $affected_rows;
	}

	/**
	 * Retorna la cantidad de microsegundos transcurridos en las operaciones de la base de datos
	 *
	 * @return float
	 */
	public function getElapsedTime() {
		return $this->_time_elapsed;
	}

	/**
	 * Retorna la cantidad de queries executadas
	 *
	 * @return int
	 */
	public function getQueryCount() {
		return $this->_query_count;
	}
	
}
