<?php

/**
 * @author jaraya
 */
final class Log {

	/**
	 * The object instance
	 *
	 * @var RoboinvestLog
	 */
	private static $_instance = null;

	private $_logFile;

	private function  __construct() {
		$this->_logFile = CODEROOT.'logs'.DS.'fondos.log';
	}

	/**
	 * Get the instance (singleton)
	 *
	 * @return Log The instance of this class
	 */
	public static function getInstance(){
		if ( !isset(self::$_instance) || self::$_instance == null ) {
			$c = __CLASS__;
			self::$_instance = new $c;
		}
		return self::$_instance;
	}


	/**
	 * Log a message into the log file
	 *
	 * @param strign $message The message to log
	 * @param bool $display If true show the message
	 */
	public function log($message, $display = false){
		$prefix = "";
		if (isset($_SESSION['notrack']) && $_SESSION['notrack'] == 'enabled') {
			$prefix .= "[notrack]";
		}
		
		if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
			$prefix .= "[{$_SERVER['REMOTE_ADDR']}]";
		}

//		if ( isset($_COOKIE['fondo_id']) ) {
//			$fondo_id = $_COOKIE['fondo_id'];
//			$fondo = Fondo::getFondoById($fondo_id)->getNombre();
//			$prefix .= "[$fondo]";
//		}
//
//		if ( isset($_COOKIE['afp_id']) ){
//			$afp_id = $_COOKIE['afp_id'];
//			$afp = AFP::getAFPById($afp_id)->getNombre();
//			$prefix .= "[$afp]";
//		}

		$current_tz =  date_default_timezone_get(); date_default_timezone_set(ini_get('date.timezone'));
		$date = date("Y/m/d H:i:s");
		date_default_timezone_set($current_tz);
		$log = $prefix."[$date] - $message\n";
		$fp = @fopen($this->_logFile,'a');
		if ( $display ) echo $prefix."$log<br/>";
		if ( is_resource($fp) ) {
			@fwrite($fp, $log);
			@fclose($fp);
		} else {
			trigger_error("Can't open '{$this->_logFile}' log file",E_USER_WARNING);
		}
	}
	
	/**
	 * 
	 * @param Exception $ex 
	 */
	public function logException (Exception $ex, $display = false) {
		$this->log("Error '{$ex->getMessage()}' . Trace:\n{$ex->getTraceAsString()}", $display);
	}

	public function log_r ($object) {
		$this->log(print_r($object, true));
	}

}
