<?php

abstract class FondosAFPObject {
	
	const CACHE_LIFETIME_1_SECOND = 1;
	const CACHE_LIFETIME_1_MINUTE = 60;
	const CACHE_LIFETIME_1_HOUR = 3600;
	const CACHE_LIFETIME_1_DAY = 86400;
	const CACHE_LIFETIME_1_MONTH = 2678400;
	const CACHE_LIFETIME_1_YEAR = 31536000;
	
	/**
	 * Indica el nombre de la tabla en la base de datos
	 * 
	 * @var string Nombre de la tabla
	 */
	protected $_table;
	
	/**
	 * Un arreglo con los campos de la base de datos y sus valores
	 *
	 * @var array El arreglo con los nombres y valores del objeto 
	 */
	protected $_fields = array();
	
	/**
	 * Indica el nombre de la columna en la base de datos que contiene la clave primaria
	 *
	 * @var string Nombre de la llave primaria
	 */
	protected $_primary_key = 'id';
	
	/**
	 * Indica el lifetime del objeto en la cache
	 *
	 * @var int lifetime del objeto 
	 */
	protected $_cache_lifetime = self::CACHE_LIFETIME_1_SECOND;

	/**
	 * Recupera los datos del objeto
	 *
	 * @param int $id Valor de la clave primaria del objeto
	 * @return FondosAFPObject Instancia del objeto
	 */
	protected function retrieve ( $id ) {
		$cache_key = $this->_table."-$id";
		
		if ( function_exists('apc_fetch')) {
			$cache_val = apc_fetch($cache_key);
			if ( $cache_val !== false ) {
				$this->_fields = $cache_val->_fields;
				return;
			}
		}
		
		$sql = "SELECT * FROM " . $this->_table . " WHERE " . $this->_primary_key . " = :id ";
		$params = array(':id' => $id);
		$fields = Database::getInstance()->getRow($sql, $params);
		foreach ($fields as $k => $v) {
			if (!array_key_exists($k, $this->_fields)) {
				throw new Exception("No existe en attributo $k en el objeto");
			}
			$this->_fields[$k] = trim($v);
		}
		if ( function_exists('apc_store') ) {
			apc_store($cache_key, $this, $this->_cache_lifetime);
		}
	}
	
}
