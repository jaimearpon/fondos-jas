<?php

/**
 * Clase para funciones de uso comun
 *
 * @author Julio Araya <julioarayacerda@gmail.com>
 */
final class Utils {

	/**
	 * Obtiene la URL de la imagen gravatar
	 *
	 * @param string $email The email address
	 * @param string $size Size in pixels, defaults to 80px [ 1 - 512 ]
	 * @param boole $img True to return a complete IMG tag False for just the URL
	 * @param array $atts Optional, additional key/value attributes to include in the IMG tag
	 * @return String containing either just a URL or a complete image tag
	 * @source http://gravatar.com/site/implement/images/php/
	 */
	public static function getGravatar( $email, $size = 80, $img = false, $atts = array() ) {
		$url = 'https://secure.gravatar.com/avatar/';
		$url .= md5( strtolower( trim( $email ) ) );
		$url .= "?s=$size&r=g";
		if ( $img ) {
			$url = '<img src="' . $url . '"';
			foreach ( $atts as $key => $val )
				$url .= ' ' . $key . '="' . $val . '"';
			$url .= ' />';
		}
		return $url;
	}


	/**
	 * Chequea si el valor es un entero positivo
	 *
	 * @param mixed $val El valor a chequear
	 * @param bool $permitirNulo Se permiten valor nulos?
	 * @return int Un entero positivo
	 * @throws Exception Si el valor dado no es un entero positivo
	 */
	public static function forzarEnteroPositivo ( $val , $permitirNulo = false ) {
		if ( $val === null && $permitirNulo === true ) return $val;
		if ( $val === '' )
			throw new Exception('Se esperaba un entero positivo');
		if ( $val === 0 ) return 0;
		$v = filter_var($val, FILTER_VALIDATE_INT, array('options' => array('min_range' => 0)));
		if ( $v === false)
			throw new Exception('Se esperaba un entero positivo');
		return $v;
	}


	/**
	 * Chequea si el valor es un entero
	 *
	 * @param mixed $val El valor a chequear
	 * @param bool $permitirNulo Se permiten valor nulos?
	 * @return int Un entero
	 * @throws Exception Si el valor dado no es un entero
	 */
	public static function forzarEntero ( $val , $permitirNulo = false ) {
		if ( $val === null && $permitirNulo === true ) return $val;
		if ( $val === '' )
			throw new Exception('Se esperaba un entero');
		if ( $val === 0 ) return 0;
		$v = filter_var($val, FILTER_VALIDATE_INT);
		if ( $v === false) 
			throw new Exception('Se esperaba un entero');
		return $v;
	}


	/**
	 * Chequea si el valor dado es un decimal positivo
	 *
	 * @param mixed $val El valor a chequear
	 * @param bool $permitirNulo Se permiten valor nulos?
	 * @return float Un decimal positivo
	 * @throws Exception Si el valor dado no es un decimal positivo
	 */
	public static function forzarDecimalPositivo ( $val , $permitirNulo = false ) {
		if ( $val === null && $permitirNulo === true ) return $val;
		if ( $val === '' )
			throw new Exception('Se esperaba un decimal positivo');
		if ( (float)$val === 0.0 ) return 0.0;
		$v = filter_var($val, FILTER_VALIDATE_FLOAT);
		if ( $v === false || $v < 0.0 )
			throw new Exception('Se esperaba un decimal positivo');
		return $v;
	}


	/**
	 * Chequea si el valor dado es un decimal
	 *
	 * @param mixed $val El valor a chequear
	 * @param bool $permitirNulo Se permiten valor nulos?
	 * @return float Un decimal
	 * @throws Exception Si el valor dado no es un decimal
	 */
	public static function forzarDecimal ( $val, $permitirNulo = false ) {
		if ( $val === null && $permitirNulo === true ) return $val;
		if ( $val === '' )
			throw new Exception('Se esperaba un decimal');
		if ( (float)$val === 0.0 ) return 0.0;
		$v = filter_var($val, FILTER_VALIDATE_FLOAT);
		if ( $v === false )
			throw new Exception('Se esperaba un decimal');
		return $v;
	}

	/**
	 * Sanitiza el valor dado como string
	 *
	 * @param mixed $val El valor a sanitizar
	 * @param bool $permitirNulo Permitir valores nulos?
	 * @return string Un string sanitizado
	 */
	public static function sanitizarString ( $val , $permitirNulo = false ) {
		if ( $val === null && $permitirNulo === true ) return $val;
		return stripslashes(trim(filter_var($val, FILTER_SANITIZE_STRING)));
	}


	/**
	 * Chequea si el valor dado es un email válido
	 *
	 * @param mixed $val El valor a chequear
	 * @param bool $permitirNulo permitir valores nulos?
	 * @return string Un email válido
	 * @throws Exception Si el valor dado no es un email válido
	 */
	public static function forzarEMailValido ( $val , $permitirNulo = false ) {
		if ( $val === null && $permitirNulo === true ) return $val;
		$email = filter_var($val, FILTER_VALIDATE_EMAIL);
		if( $email === false ) {
			throw new Exception('Por favor ingresa un email válido');
		}
		return $email;
	}

	/**
	 * Chequea si el valor dado coincide sobre alguno de los valores aceptados
	 *
	 * @param mixed $val The value to check
	 * @param array $acceptedValues An enumeration with the accepted values
	 * @param string $message The message to throw if there is any exception
	 * @param bool $allowNullValue Allow null value?
	 * @return mixed The given value if this match against any of the accepted values
	 * @throws RoboinvestException If the given value doesn't match at all
	 */
	public static function forzarValorEnum ( $val, $valoresAceptados, $mensajeDeError , $permitirNulo = false ) {
		if ( $val === null && $permitirNulo === true ) return $val;
		if ( is_array($valoresAceptados) === true ) {
			if ( !in_array( $val, $valoresAceptados ) )
				throw new Exception($mensajeDeError);
		} else throw new Exception(__LINE__);
		return $val;
	}

	/**
	 * Genera un string aleatorio
	 *
	 * @param int $length
	 * @return string 
	 */
	public static function randomString( $length ) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";	
		$size = strlen( $chars );
		$str = '';
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
		return $str;
	}
	
	/**
	 * Entrega el hash de un archivo de acuerdo a la fecha de modificacion de éste
	 * 
	 * @param string $fileName Ruta del archivo
	 * @return string 
	 */
	public static function fileHash( $fileName ) {
		if (file_exists($fileName) ) {
			return md5(filemtime($fileName));
		}
		return '';
	}
	
	/**
	 * Realiza una redireccion
	 *
	 * @param string $url
	 * @param int $http_code El codigo http
	 */
	public static function redirect ( $url, $http_code = 301 ) {
		header("Location: $url", $http_code);
		exit;
	}
	
	
	public static function renderTableFondos ($afp_name, $info) {
		$a12 = $info['A']['12_meses'] < 0 ? 'red' : 'green';
		$b12 = $info['B']['12_meses'] < 0 ? 'red' : 'green';
		$c12 = $info['C']['12_meses'] < 0 ? 'red' : 'green';
		$d12 = $info['D']['12_meses'] < 0 ? 'red' : 'green';
		$e12 = $info['E']['12_meses'] < 0 ? 'red' : 'green';

		$a6 = $info['A']['6_meses'] < 0 ? 'red' : 'green';
		$b6 = $info['B']['6_meses'] < 0 ? 'red' : 'green';
		$c6 = $info['C']['6_meses'] < 0 ? 'red' : 'green';
		$d6 = $info['D']['6_meses'] < 0 ? 'red' : 'green';
		$e6 = $info['E']['6_meses'] < 0 ? 'red' : 'green';
		
		$a3 = $info['A']['3_meses'] < 0 ? 'red' : 'green';
		$b3 = $info['B']['3_meses'] < 0 ? 'red' : 'green';
		$c3 = $info['C']['3_meses'] < 0 ? 'red' : 'green';
		$d3 = $info['D']['3_meses'] < 0 ? 'red' : 'green';
		$e3 = $info['E']['3_meses'] < 0 ? 'red' : 'green';
		
		$am = $info['A']['este_mes'] < 0 ? 'red' : 'green';
		$bm = $info['B']['este_mes'] < 0 ? 'red' : 'green';
		$cm = $info['C']['este_mes'] < 0 ? 'red' : 'green';
		$dm = $info['D']['este_mes'] < 0 ? 'red' : 'green';
		$em = $info['E']['este_mes'] < 0 ? 'red' : 'green';
		
		$aa = $info['A']['este_anyo'] < 0 ? 'red' : 'green';
		$ba = $info['B']['este_anyo'] < 0 ? 'red' : 'green';
		$ca = $info['C']['este_anyo'] < 0 ? 'red' : 'green';
		$da = $info['D']['este_anyo'] < 0 ? 'red' : 'green';
		$ea = $info['E']['este_anyo'] < 0 ? 'red' : 'green';
		
		$AFP_Name = ucfirst($afp_name);
		$table = <<<TABLE_
		<table class="informe">
			<tr>
				<th>&nbsp;</th>
				<th class="cuota"><a href="http://{$_SERVER['SERVER_NAME']}/fondos/afp-$afp_name/fondo-a/" title="Rentabilidad del fondo A de la AFP $AFP_Name">Fondo A</a></th>
				<th class="cuota"><a href="http://{$_SERVER['SERVER_NAME']}/fondos/afp-$afp_name/fondo-b/" title="Rentabilidad del fondo B de la AFP $AFP_Name">Fondo B</a></th>
				<th class="cuota"><a href="http://{$_SERVER['SERVER_NAME']}/fondos/afp-$afp_name/fondo-c/" title="Rentabilidad del fondo C de la AFP $AFP_Name">Fondo C</a></th>
				<th class="cuota"><a href="http://{$_SERVER['SERVER_NAME']}/fondos/afp-$afp_name/fondo-d/" title="Rentabilidad del fondo D de la AFP $AFP_Name">Fondo D</a></th>
				<th class="cuota"><a href="http://{$_SERVER['SERVER_NAME']}/fondos/afp-$afp_name/fondo-e/" title="Rentabilidad del fondo E de la AFP $AFP_Name">Fondo E</a></th>
			</tr>
			<tr>
				<td>&Uacute;ltimos 12 meses</td>
				<td class="aligncenter $a12">{$info['A']['12_meses']}%</td>
				<td class="aligncenter $b12">{$info['B']['12_meses']}%</td>
				<td class="aligncenter $c12">{$info['C']['12_meses']}%</td>
				<td class="aligncenter $d12">{$info['D']['12_meses']}%</td>
				<td class="aligncenter $e12">{$info['E']['12_meses']}%</td>
			</tr>
			<tr>
				<td>&Uacute;ltimos 6 meses</td>
				<td class="aligncenter $a6">{$info['A']['6_meses']}%</td>
				<td class="aligncenter $b6">{$info['B']['6_meses']}%</td>
				<td class="aligncenter $c6">{$info['C']['6_meses']}%</td>
				<td class="aligncenter $d6">{$info['D']['6_meses']}%</td>
				<td class="aligncenter $e6">{$info['E']['6_meses']}%</td>
			</tr>
			<tr>
				<td>&Uacute;ltimos 3 meses</td>
				<td class="aligncenter $a3">{$info['A']['3_meses']}%</td>
				<td class="aligncenter $b3">{$info['B']['3_meses']}%</td>
				<td class="aligncenter $c3">{$info['C']['3_meses']}%</td>
				<td class="aligncenter $d3">{$info['D']['3_meses']}%</td>
				<td class="aligncenter $e3">{$info['E']['3_meses']}%</td>
			</tr>
			<tr>
				<td>Este mes</td>
				<td class="aligncenter $am">{$info['A']['este_mes']}%</td>
				<td class="aligncenter $bm">{$info['B']['este_mes']}%</td>
				<td class="aligncenter $cm">{$info['C']['este_mes']}%</td>
				<td class="aligncenter $dm">{$info['D']['este_mes']}%</td>
				<td class="aligncenter $em">{$info['E']['este_mes']}%</td>
			</tr>
			<tr>
				<td>Este a&ntilde;o</td>
				<td class="aligncenter $aa">{$info['A']['este_anyo']}%</td>
				<td class="aligncenter $ba">{$info['B']['este_anyo']}%</td>
				<td class="aligncenter $ca">{$info['C']['este_anyo']}%</td>
				<td class="aligncenter $da">{$info['D']['este_anyo']}%</td>
				<td class="aligncenter $ea">{$info['E']['este_anyo']}%</td>
			</tr>
		</table>
TABLE_;
		return $table;
	}
	
}
