<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1,2,3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Mantiene los datos de las afp
 *
 * @author Julio Araya C <julioarayaceda@gmail.com>
 */
class AFP  extends FondosAFPObject {

	const TABLE = 'afp';
	public $_table = 'afp';
	
	protected $_primary_key = 'afp_id';
	
	protected $_cache_key = 'afp';

	public $_fields = array(
		'afp_id' => NULL,
		'nombre' => NULL,
		'descripcion' => NULL,
		'api_name' => NULL,
		'country' => NULL,
		'status' => NULL
	);

	private static $_afps = null;
	
	public function getID () {
		return $this->_fields[$this->_primary_key];
	}
	
	public function getNombre () {
		return $this->_fields['nombre'];
	}
	
	public function getDescripcion () {
		return $this->_fields['descripcion'];
	}
	
	public function getAPIName () {
		return trim($this->_fields['api_name']);
	}
	
	public function getCountry() {
		return $this->_fields['country'];
	}
	
	public function getStatus () {
		return $this->_fields['status'];
	}
	
	public function __construct( $afp_id = null ) {
		$v = filter_var($afp_id, FILTER_VALIDATE_INT, array('options' => array('min_range' => 0)));
		if ( $v !== false) {
			$this->retrieve($v);
		}
	}

	/**
	 * Retorna todas las AFPs
	 *
	 * @return AFP[] 
	 */
	public static function getAFPs ($country = 'CL', $status = 1) {
		if (self::$_afps == null) {
			self::$_afps = array();
			$sql = "SELECT * FROM " . self::TABLE . "  WHERE country = :country AND status = :status ORDER BY nombre ASC";
			$params = array(
				':country' => $country,
				':status' => $status
			);
			$rows = Database::getInstance()->query($sql, $params);
			
			foreach ( $rows as $row ) {
				$afp = new AFP();
				foreach ($row as $k => $v) {
					$afp->_fields[$k] = trim($v);
				}
				array_push(self::$_afps, $afp);
			}
		}
		return self::$_afps;
	}
	
	public static function getDefaultAFP () {
		$cookie_name = $_SERVER['HTTP_HOST'].'.afp';
		$defaultAFP = null;
		if ( isset($_COOKIE[$cookie_name]) ){
			$defaultAFP = $_COOKIE[$cookie_name];
		}
		if ( isset($_POST['afp']) ) {
			$defaultAFP = $_POST['afp'];
		}
		@setcookie($cookie_name, $defaultAFP, time()+84600*30, '/', $_SERVER['SERVER_NAME']);
		return $defaultAFP;
		
	}

	/**
	 * Returns an AFP by the Id
	 *
	 * @param int $AFPId
	 * @return AFP
	 * @todo Recuperar la AFP de la cache o si no agregarla al array
	 */
	public static function getAFPById ($AFPId) {
		if ($AFPId < 0 ) {
			$AFPId = 0;
		}
		if ($AFPId > 6) {
			$AFPId = 6;
		}
		if (self::$_afps== null) {
			self::getAFPs();
		}
		return self::$_afps[$AFPId];
	}
	
	/**
	 * Retorna el Id de la AFP a partir de su nombre
	 * 
	 * @param string $name
	 * @return int
	 */
	public static function getAFPByAPIName ($api_name, $country = 'CL', $status = 1) {
		if (self::$_afps== null) {
			self::getAFPs($country, $status);
		}
		foreach (self::$_afps as $afp) {
			if (trim($afp->_fields['api_name']) == trim($api_name)) {
				return $afp;
			}
		}
		Log::getInstance()->log(__CLASS__."::".__LINE__);
		return NULL;
	}
	
}
