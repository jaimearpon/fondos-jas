<?php

/**
 * Clase para el manejo de las cuotas de los fondos de pensiones
 *
 * @author Julio Araya Cerda <julio.araya@fondosafp.cl>
 */
class Cuota {
	
	const TABLE = 'cuotas';
	
	public static function getCuotasAsArray ( $afp_id, $fondos, $desde, $hasta, $type ) {
		if ( $type == 'porcentaje' ) {
			$results = array();
			foreach ( $fondos as $i => $fondo_id ) {
				$params = array(
					':afp_id' => $afp_id,
					':fondo_id' => $fondo_id,
					':fecha_desde' => $desde,
					':fecha_hasta' => $hasta
				);
				$sql = 'select * from variacionporcentual_api(:fecha_desde,:fecha_hasta,:afp_id,:fondo_id)';
				$rows = Database::getInstance()->query($sql, $params);
				foreach ($rows as $row) {
					$results[] = $row;
				}
			}
			return $results;
		} elseif ($type == 'valor') {
			$params = array(
				':afp_id' => $afp_id,
				':fecha_desde' => $desde,
				':fecha_hasta' => $hasta
			);
			$sql  = ' SELECT Cuota.fondo_id, Fondo.api_name, Cuota.valor, Cuota.fecha 
				FROM ' . self::TABLE . ' Cuota
				INNER JOIN '.Fondo::TABLE.' Fondo ON (Fondo.fondo_id = Cuota.fondo_id) 
			WHERE 
				Cuota.afp_id = :afp_id AND
				Cuota.fecha >= :fecha_desde AND 
				Cuota.fecha <= :fecha_hasta AND 
				Cuota.fondo_id IN (' 
			;
			
			foreach ( $fondos as $i => $fondo_id ) {
				$sql .= ' :fondo_' . $i . ',' ;
				$params[':fondo_'.$i] = $fondo_id;
			}
			$sql = substr($sql, 0, -1);
			$sql .= ') ORDER BY Cuota.fondo_id ASC, Cuota.fecha ASC';
			return Database::getInstance()->query($sql, $params);
		} elseif ($type == 'patrimonio') {
			$params = array(
				':afp_id' => $afp_id,
				':fecha_desde' => $desde,
				':fecha_hasta' => $hasta
			);
			$sql  = ' SELECT Cuota.fondo_id, Fondo.api_name, Cuota.valor, Cuota.patrimonio, Cuota.fecha 
				FROM ' . self::TABLE . ' Cuota 
				INNER JOIN '.Fondo::TABLE.' Fondo ON (Fondo.fondo_id = Cuota.fondo_id) 
				WHERE 
					Cuota.afp_id = :afp_id AND 
					Cuota.fecha >= :fecha_desde AND 
					Cuota.fecha <= :fecha_hasta AND 
					Cuota.fondo_id IN (' ;
			foreach ( $fondos as $i => $fondo_id ) {
				$sql .= ' :fondo_' . $i . ',' ;
				$params[':fondo_'.$i] = $fondo_id;
			}
			$sql = substr($sql, 0, -1);
			$sql .= ') ORDER BY Cuota.fondo_id ASC, Cuota.fecha ASC';
			return Database::getInstance()->query($sql, $params);
		}
	}
	
	public static function getUltimoValor ($afp_id, $fondo_id ) {
		$sql = 'SELECT fecha, valor FROM '.self::TABLE.' WHERE afp_id = :afp_id AND fondo_id = :fondo_id ORDER BY fecha DESC LIMIT 1';
		$params = array(
		    ':afp_id' => $afp_id,
		    ':fondo_id' => $fondo_id
		);
		return Database::getInstance()->getRow($sql, $params);
	}
	
	public static function getPorcentage ( $desde, $afp, $fondo, $descripcion ) {
		$hoy   = date("Y-m-d");
		$desde = date("Y-m-d", $desde);
		$key = md5("porcentage-$hoy-$desde-$afp-$fondo-$descripcion");
		if ( function_exists('apc_fetch')) {
			$apc_val = apc_fetch($_SERVER['SERVER_NAME'].'_'.$key);
			if ( $apc_val !== false )
				return number_format ($apc_val,2);
		}
		
		$afps = array(
			'capital'   => 1,
			'cuprum'    => 2,
			'habitat'   => 3,
			'modelo'    => 4,
			'planvital' => 5,
			'provida'   => 6
		);
		$fondos = array(
			'A' => 1,
			'B' => 2,
			'C' => 3,
			'D' => 4,
			'E' => 5
		);
		
		//valor inicial
		$sql = 'SELECT valor FROM ' . Cuota::TABLE . ' WHERE fecha = :fecha AND afp_id = :afp_id AND fondo_id = :fondo_id';
		$params = array(
			':fecha' => $desde,
			':afp_id' => $afps[$afp],
			':fondo_id' => $fondos[$fondo]
		);
		$valor_inicial = Database::getInstance()->getOne($sql, $params);
		//valor final
		$sql = 'SELECT valor FROM ' . Cuota::TABLE . ' WHERE afp_id = :afp_id AND fondo_id = :fondo_id ORDER BY fecha DESC LIMIT 1';
		$params = array(
			':afp_id' => $afps[$afp],
			':fondo_id' => $fondos[$fondo]
		);
		$valor_final = Database::getInstance()->getOne($sql, $params);
		$porcentage = 0;
		if ( $valor_final && $valor_inicial ) {
			$porcentage = ( ($valor_final-$valor_inicial) / $valor_inicial ) * 100;
			if ( function_exists('apc_store') )
				apc_store($_SERVER['SERVER_NAME'].'_'.$key, $porcentage, 10800);
		}
		return number_format($porcentage,2);
	}
	
}
