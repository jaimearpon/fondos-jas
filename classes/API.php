<?php

/**
 * Description of API
 *
 * @author Julio Araya <julio.araya@fondosafp.cl>
 */
final class API {

	private $headers = NULL;
	
	/**
	 * La instancia de la clase
	 *
	 * @var API
	 */
	private static $_instance = null;

	private function __construct () {
		$isJsonP = isset($_GET['callback']);
		$this->headers = array(
			//'X-API-Type' => 'REST',
			'X-API-Version' => '1.0 beta',
			//'X-API-OAuth' => 'required',
			//'X-API-OAuth-Version' => '1.0',
			//'X-API-OAuth-RequestToken' => SERVER_URL . 'api/request_token.php',
			//'X-API-OAuth-AccessToken' => SERVER_URL . 'api/access_token.php',
			//'X-API-OAuth-Application-Requests' => SERVER_URL . 'api/api_requests.php',
			'X-API-Services' => SERVER_URL . 'api/1.0/services.json',
			'X-API-Provider' => 'Fondos AFP (www.fondosafp.com)',
			'X-Powered-By' => 'FondosAFP API',
			'X-Server-Timestamp' => time(),
			'Content-Type' => $isJsonP ? 'text/javascript' : 'application/json',
			'Server' => 'FondosAFP API ('.SERVER_URL .'api/)'
		);
		foreach ($this->headers as $key => $value) {
			header("$key: $value");
		}
	}

	/**
	 * Obtiene la instancia de la clase (singleton)
	 *
	 * @return API La instancia de esta clase
	 */
	public static function getInstance(){
		if ( !isset(self::$_instance) || self::$_instance == null ) {
			$c = __CLASS__;
			self::$_instance = new $c;
		}
		return self::$_instance;
	}

	public function processRequest ($version, $endpoint, $get, $post, $server) {
		Log::getInstance()->log("[API-$version] $endpoint?get=".json_encode($get).'&post='.  json_encode($post));
		$public_endpoints = array('', 'services.json');
		$is_unavailable = false;
		if (strpos($server['HTTP_REFERER'], $server['HTTP_HOST']) === FALSE) {
			$is_unavailable = true;
		}
		
		if ($version != '1.0') {
			Log::getInstance()->log("[API-$version][bad_request] $endpoint?get=".json_encode($get).'&post='.  json_encode($post));
			return $this->_bad_request('API version incorrect');
		}
		
		//Enviar un correo, alguien está tratando de usar la API sin autorizacion
		if ($is_unavailable) {
			AmazonSES::getInstance()->sendEmail(AWS_EMAIL_CONTACTO, AWS_EMAIL_CONTACTO, "API no disponible", "<pre>SERVER: ".print_r($server, true)."\nGET: ".print_r($get, true)."\nPOST: ".print_r($post, true)."\n</pre>");
			$is_unavailable = false;
		}
		
		if (!in_array($endpoint, $public_endpoints) && $is_unavailable) {
			Log::getInstance()->log("[API-$version][unavailable] $endpoint?get=".json_encode($get).'&post='.  json_encode($post));
			return $this->_service_unavailable('beta');
		}
		
		switch ($endpoint) {
			case '':
				$response = $this->_api_options();
				break;
			case 'services.json':
				$response = $this->_api_services();
				break;
			case 'fondos.json':
				$response = $this->_api_fondos($get, $post, $server);
				break;
			default:
				$response = $this->_bad_request('Endpoint not available');
				break;
		}
		return $response;
	}
	
	private function _service_unavailable($reason) {
		header("HTTP/1.0 503 Service Unavailable");
		$reasons = array(
			'beta' => 'El servicio está en estado beta, para mas información escriba a api@fondosafp.cl'
		);
		$response = array(
			'response' => array(
				'status' => 503,
				'message' => 'HTTP/1.1 503 Service Unavailable'
			),
			'headers' => $this->headers,
			'reason' => $reasons[$reason]
		);
		return json_encode($response);
	}

	private function _api_options () {
		$response = array(
			'response' => array(
				'status' => 200,
				'message' => 'HTTP/1.1 200 OK'
			),
			'headers' => $this->headers
		);
		return json_encode($response);
	}

	private function _bad_request($errors = FALSE) {
		header("HTTP/1.0 400 Bad Request");
		$response = array(
			'response' => array(
				'status' => 400,
				'message' => 'HTTP/1.1 Bad Request',
			),
			'headers' => $this->headers,
		);
		if ($errors !== FALSE) {
			$response['errors'] = $errors;
		}

		AmazonSES::getInstance()->sendEmail(AWS_EMAIL_CONTACTO, AWS_EMAIL_CONTACTO, "API Bad Request", "<pre>response: ".json_encode($response)."\n"."SERVER: ".print_r($_SERVER, true)."\nGET: ".print_r($_GET, true)."\nPOST: ".print_r($_POST, true)."\n</pre>");
		return json_encode($response);
	}

	private function _api_services () {
		$response = array(
			'response' => array(
				'status' => 200,
				'message' => 'HTTP/1.1 OK'
			),
			'headers' => $this->headers,
			'results' => array(
				'services' => array(
					'fondos.json' => array(
						'url' => SERVER_URL.'api/1.0/fondos.json',
						'format' => 'json',
						'documentation_url' => '',
						//'max_requests_per_second' => 1,
						'parameters' => array(
							'afp' => array(
								'type' => 'string',
								'required' => 'yes',
								'options' => array('CAPITAL', 'CUPRUM', 'HABITAT', 'MODELO', 'PLANVITAL', 'PROVIDA'),
								'multiple' => 'no'
							),
							'fondos' => array(
								'type' => 'string',
								'required' => 'yes',
								'options' => array('A', 'B', 'C', 'D','E'),
								'multiple' => 'yes'
							),
							'from' => array(
								'type' => 'string',
								'required' => 'no',
								'format' => 'YYYY-MM-DD',
								//'min_date' => '2002-01-01',
								//'max_date' => date("Y-m-d"),
								'multiple' => 'no',
								//'restrictions' => 'from <= until',
							),
							'until' => array(
								'type' => 'string',
								'required' => 'no',
								'format' => 'YYYY-MM-DD',
								//'min_date' => '2002-01-01',
								//'max_date' => date("Y-m-d"),
								'multiple' => 'no',
								//'restrictions' => 'until >= from'
							)
							,
							'type' => array(
								'type' => 'string',
								'required' => 'no',
								'options' => array('valor', 'porcentaje'/*, 'patrimonio'*/),
								'multiple' => 'no'
							),
							'hideFDS' => array(
								'type' => int,
								'required' => 'no',
								'options' => array(0,1),
								'multiple' => 'no'
							)
						)
					)
				),
			)
		);
		return json_encode($response);
	}

	private function _api_fondos ($get, $post, $server) {
		if (!array_key_exists('afp', $get)) {
			return $this->_bad_request('Parameter afp is required');
		}

		if (!array_key_exists('fondos', $get)) {
			return $this->_bad_request('Parameter fondos is required');
		}

		//parameter afp
		$param_afp = $get['afp'];
		$valid_afps = array('CAPITAL', 'CUPRUM', 'HABITAT', 'MODELO', 'PLANVITAL', 'PROVIDA');
		if (!in_array($param_afp, $valid_afps)) {
			return $this->_bad_request('Invalid parameter afp \''.$param_afp.'\', accepted values: ' . implode(', ', $valid_afps));
		}
		$afp = AFP::getAFPByAPIName($param_afp);
		$afp_id = $afp->getID();
		
		//parameter fondo
		$param_fondos = explode(',', $get['fondos']);
		$valid_fondos = array('A', 'B', 'C', 'D', 'E');
		$fondos = array();
		foreach ($param_fondos as $param_fondo) {
			if (!in_array($param_fondo, $valid_fondos)) {
				return $this->_bad_request('Invalid parameter fondos \''.$param_fondo.'\', accepted values: ' . implode(', ', $valid_fondos));
			}
			$fondo_id = Fondo::getFondoIDByAPIName($param_fondo);
			$fondos[] = $fondo_id;
		}
		
		//parameters desde, hasta
		$from = isset($get['from']) ? $get['from'] : '';
		$until = isset($get['until']) ? $get['until'] : '';
		$d = gmdate("Y-m-d", strtotime("-6 months"));
		$h = gmdate("Y-m-d", strtotime("now"));
		if ( strlen($from) ) $d = $from;
		if ( strlen($until) ) $h = $until;
		
		//parameter tipo
		$param_type   = isset($get['type']) ? $get['type'] : 'valor';
		$valid_types = array('valor', 'porcentaje', 'patrimonio');
		if (!in_array($param_type, $valid_types)) {
			return $this->_bad_request('Invalid parameter type \''.$param_type.'\', accepted values: ' . implode(', ', $valid_types));
		}

		//parameter fds
		$param_fds = isset($get['hideFDS']) ? (int)$get['hideFDS'] : 0;
		$valid_fds = array(0,1);
		if (!in_array($param_fds, $valid_fds)) {
			return $this->_bad_request('Invalid parameter hideFDS \''.$param_fds.'\', accepted values: ' . implode(', ', $valid_fds));
		}

		$rows = Cuota::getCuotasAsArray($afp_id, $fondos, $d, $h, $param_type);

		$data = array();
		foreach ($rows as $row) {
			if ($param_fds == 1 && Grafico::isWeekend($row['fecha'])) {
				continue;
			}
			$valor = (float)$row['valor'];
			if ($param_type == 'patrimonio') {
				$valor = (float)number_format(($row['patrimonio'])/1000000000000,3);
			}
			$data[$row['api_name']][] = array(
				(float)((string)strtotime($row['fecha']).'000'), $valor
			);
		}
		
		if ( count ($rows) == 0 ) {
			Log::getInstance()->log("[fondos] Sin datos");
			return $this->_bad_request('Sin datos');
		}
		
		$hash = strtolower(sha1(base64_encode($afp_id.'/**/*-'.implode('asdf asd.', $fondos).'fart25b'.$desde.'v25tc25'.$hasta.'356bscgaqwerg	35'.count($rows).$type.($ocultarFDS?'1':'0'))));
		Log::getInstance()->log("[fondos][$hash][$desde/$hasta](afp: $afp_id fondos: ".implode(',', $fondos).")");
		
		$details_data = array();
		foreach ( $rows as $row ) {
			if ($param_type == 'patrimonio') {
				$valor = (float)number_format(($row['patrimonio'])/1000000000000,3);
				$details_data[$row['fondo_id']][$row['fecha']] = $valor;
			} else {
				$valor = (float)$row['valor'];
				$details_data[$row['fondo_id']][$row['fecha']] = $valor;
			}
		}

		$minY = 100000000000000;
		$maxY = 0;
		$details = array();
		foreach ( $details_data as $fondo_id => $d ) {
			if ( min($d) < $minY ) $minY = min($d);
			if ( max($d) > $maxY ) $maxY = max($d);
			
			$fondo = new Fondo($fondo_id);
			$ini = array_slice($d, 0, 1);
			$fin = array_slice($d, count($d)-1, 1);

			$fecha_valor_inicial = key($ini);
			$fecha_valor_final = key($fin);
			$valor_inicial = $ini[$fecha_valor_inicial];
			$valor_final = $fin[$fecha_valor_final];
			
			$details[$fondo->getAPIName()] = array(
				'afp' => $afp->getNombre(),
				'descripcion' => $fondo->getDescripcion(),
				'valor_inicial' => $valor_inicial,
				'valor_final' => $valor_final,
				'fecha_valor_inicial' => $fecha_valor_inicial,
				'fecha_valor_final' => $fecha_valor_final,
				'variacion_real' => number_format($valor_final - $valor_inicial,2),
				'variacion_porcentual' => @number_format( (($valor_final - $valor_inicial)/$valor_inicial)*100, 3),
				'data' => $data[$fondo->getAPIName()]
			);
		}

		$prefix = 'Valor cuota';
		switch ($param_type) {
			case 'patrimonio':
				$prefix = 'Patrimonio en billones de pesos';
				break;
			case 'porcentaje':
				$prefix = 'Variación porcentual del valor cuota';
				break;
			default:
				$prefix = 'Valor cuota';
				break;
		}
		
		$response = array(
			'response' => array(
				'status' => 200,
				'message' => 'HTTP/1.1 200 OK'
			),
			'headers' => $this->headers,
			'description' => "$prefix de la {$afp->getDescripcion()}",
			'afp' => $afp->getDescripcion(),
			'periodo' => "Desde el $from al $until" . ($param_type == 'patrimonio' ? ' (BB: billones de pesos)' : ''),
			'fondos' => $details
		);
		
		if(isset($get['callback'])) {
			$callback = $get['callback'];
			return "$callback(" . json_encode($response).");";
		} else {
			return json_encode($response);
		}
		
		
	}

}
