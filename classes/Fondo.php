<?php

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1,2,3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Mantiene los datos de los fondos
 *
 * @author Julio Araya C <julioarayaceda@gmail.com>
 */
class Fondo extends FondosAFPObject {

	/**
	 * Contiene un arreglo de objetos de tipo Fondo
	 *
	 * @var Fondo[] Fondos de la base de datos
	 */
	private static $_fondos = null;
	
	const TABLE = 'fondo';
	
	public $_table = 'fondo';
	
	protected $_primary_key = 'fondo_id';
	
	protected $_cache_key = 'fondo';

	protected $_fields = array(
		'fondo_id' => NULL,
		'nombre' => NULL,
		'descripcion' => NULL,
		'api_name' => NULL,
		'country' => NULL,
		'status' => NULL
	);
	
	public function getID () {
		return $this->_fields[$this->_primary_key];
	}
	
	public function getNombre () {
		return $this->_fields['nombre'];
	}
	
	public function getDescripcion () {
		return $this->_fields['descripcion'];
	}
	
	public function getAPIName() {
		return trim($this->_fields['api_name']);
	}
	
	public function getCountry() {
		return $this->_fields['country'];
	}
	
	public function getStatus () {
		return $this->_fields['status'];
	}
	
	public function __construct( $fondo_id = null ) {
		$v = filter_var($fondo_id, FILTER_VALIDATE_INT, array('options' => array('min_range' => 0)));
		if ( $v !== false) {
			$this->retrieve($v);
		}
	}
	
	/**
	 * Retorna todos los fondos
	 *
	 * @return Fondo[]
	 */
	public static function getFondos ($country = 'CL', $status = 1) {
		if (self::$_fondos == null) {
			self::$_fondos = array();
			$sql = "SELECT * FROM " . self::TABLE . "  WHERE country = :country AND status = :status ORDER BY nombre ASC";
			$params = array(
				':country' => $country,
				':status' => $status
			);
			$rows = Database::getInstance()->query($sql, $params);
			
			foreach ( $rows as $row ) {
				$fondo = new Fondo();
				foreach ($row as $k => $v) {
					$fondo->_fields[$k] = trim($v);
				}
				array_push(self::$_fondos, $fondo);
			}
		}
		return self::$_fondos;
	}
	
	/**
	 * Obtiene los fondos seleccionados por defecto por el usuario actual
	 * 
	 * @return type
	 */
	public static function getDefaultFondo() {
		$cookie_name = "{$_SERVER['HTTP_HOST']}.fondo_id";
		$default = null;
		if ( isset($_COOKIE[$cookie_name]) )
			$default = $_COOKIE[$cookie_name];
		if ( isset($_POST['fondo']) ) 
			$default = $_POST['fondo'];
		
		setcookie($cookie_name, $default, time()+84600*30, '/', $_SERVER['SERVER_NAME']);
		return $default;
	}
	
	public static function getDefaultFondos ($defaults = array('A','E')) {
		$cookie_name = "{$_SERVER['HTTP_HOST']}.fondos";
		$fondos_ids = array();
		if ( isset($_COOKIE[$cookie_name])) {
			$fondos_ids = explode(',',$_COOKIE[$cookie_name]);
		} else {
			$fondos_ids = $defaults;
		}
		setcookie($cookie_name, implode(',',$fondos_ids), time()+86400*30, '/', $_SERVER['SERVER_NAME']);
		return $fondos_ids;
	}
	
	
	public static function getValorFondos () {
		$valores = array();
		$afps = array(1,2,3,4,5,6);
		$fondos = array(1,2,3,4,5);
		foreach ( $afps as $afp_id) {
			$valores["$afp_id"] = array();
			foreach ( $fondos as $fondo_id ) {
				$data = Cuota::getUltimoValor($afp_id, $fondo_id);
				$valores["$afp_id"]["$fondo_id"] = $data['valor'];
			}
		}
		return $valores;
	}
	
	/**
	 * Returns a cached Fondo object by the id
	 *
	 * @param int $fondoId
	 * @return Fondo
	 */
	public static function getFondoById ($fondoId) {
		if ($fondoId < 0) {
			$fondoId = 0;
		}

		if ($fondoId > 5){
			$fondoId = 5;
		}

		if (self::$_fondos == null) {
			self::getFondos();
		}
		return self::$_fondos[$fondoId];
	}
	
	
	public static function getFondoIDByAPIName ($name) {
		if (self::$_fondos== null) {
			self::getFondos();
		}
		foreach (self::$_fondos as $fondo) {
			if ($fondo->getAPIName() == $name) {
				return $fondo->getID();
			}
		}
		return 0;
	}
	
}
