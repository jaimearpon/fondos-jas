<?php

class FondoObject {
	
	/**
	 * Identificador del objecto
	 * 
	 * @var int 
	 */
	protected $_id;
	
	/**
	 * Fecha de creacion del registro
	 *
	 * @var string
	 */
	protected $_created;
	
	/**
	 * Fecha de modificación del registro 
	 * 
	 * @var string 
	 */
	protected $_modified;
	
	
	/**
	 * Devuelve el identificador del objecto
	 *
	 * @return int
	 */
	public function getID () {
		return $this->_id;
	}
	
	/**
	 * Retorna la fecha de creacion del registro 
	 * 
	 * @return string
	 */
	public function getCreated () {
		return $this->_created;
	}
	
	/**
	 * Retorna la fecha de modificacion del registro
	 * 
	 * @return string
	 */
	public function getModified () {
		return $this->_modified;
	}
	
}
