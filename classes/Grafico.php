<?php

require_once(LIBCHART_DIR.'libchart.php');

/**
 * Genera y guarda en caché los gráficos de los fondos
 *
 * @author delpho
 */
class Grafico {

	/**
	 * Genera un gráfico de los fondos de una AFP en particular en el rango de fecha seleccionado
	 *
	 * @param int $afp_id Id de la AFP
	 * @param array $fondos id de los fondos
	 * @param string $desde Fecha mínima
	 * @param string $hasta Fecha máxima
	 */
	public static function crearGraficoFondos($afp_id, $fondos, $desde, $hasta, &$details, $type, $ocultarFDS = false) {
		$tmp = array();
		foreach ($fondos as $api_name) {
			$fondo_id = Fondo::getFondoIDByAPIName($api_name);
			$tmp[] = $fondo_id;
		}
		
		$rows = Cuota::getCuotasAsArray($afp_id, $tmp, $desde, $hasta, $type);
	
		if ( count ($rows) == 0 ) {
			Log::getInstance()->log("[fondos] Sin datos");
			return array();
		}
		
		$hash = strtolower(sha1(base64_encode($afp_id.'/**/*-'.implode('asdf asd.', $fondos).'fart25b'.$desde.'v25tc25'.$hasta.'356bscgaqwerg	35'.count($rows).$type.($ocultarFDS?'1':'0'))));
		Log::getInstance()->log("[fondos][$hash][$desde/$hasta](afp: $afp_id fondos: ".implode(',', $fondos).")");
		
		$data = array();
		$api_data = array();
		foreach ( $rows as $row ) {
			if ($ocultarFDS && self::isWeekend($row['fecha'])) {
				continue;
			}
			if ($type == 'patrimonio') {
				$valor = number_format(($row['patrimonio'])/1000000000000,3);
				$data[$row['fondo_id']][$row['fecha']] = $valor;
			} else {
				$valor = (float)$row['valor'];
				$fecha_timestamp = (string)strtotime($row['fecha']).'000';
				$data[$row['fondo_id']][$row['fecha']] = $valor;
				$api_data[$row['api_name']][] = array($fecha_timestamp, $valor);
			}
		}

		$minY = 1000000;
		$maxY = 0;
		foreach ( $data as $fondo_id => $d ) {
			if ( min($d) < $minY ) $minY = min($d);
			if ( max($d) > $maxY ) $maxY = max($d);
			
			$fondo = new Fondo($fondo_id);
			$ini = array_slice($d, 0, 1);
			$fin = array_slice($d, count($d)-1, 1);
			
			$fecha_valor_inicial = key($ini);
			$fecha_valor_final = key($fin);
			$valor_inicial = $ini[$fecha_valor_inicial];
			$valor_final = $fin[$fecha_valor_final];
			
			$details['fondos'][$fondo->getAPIName()] = array(
				'descripcion' => $fondo->getDescripcion(),
				'valor_inicial' => $valor_inicial,
				'valor_final' => $valor_final,
				'fecha_valor_inicial' => $fecha_valor_inicial,
				'fecha_valor_final' => $fecha_valor_final,
				'variacion_real' => number_format($valor_final - $valor_inicial,2),
				'variacion_porcentual' => @number_format( (($valor_final - $valor_inicial)/$valor_inicial)*100, 3),
				'data' => json_encode($api_data[$fondo->getAPIName()])
			);
		}
		
		return $details;
	}

	/**
	 * Comprueba si una fecha es sábado o domingo
	 *
	 * @param string $date
	 * @return bool
	 */
	public static function isWeekend($date) {
		$day = date('N', strtotime(str_replace("-", "/", $date)));
		return $day >= 6;
	}
	
}
