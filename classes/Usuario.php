<?php

/**
 * Clase para el manejo de usuarios
 *
 * @author Julio Araya C. <julio.araya@fondosafp.cl>
 */
final class Usuario extends FondoObject{
	
	const TABLE = 'usuario';
	
	private $_email;
	
	private $_nombre;
	
	private $_pass;
	
	/**
	 * Devuelve el email del usuario
	 * 
	 * @return string 
	 */
	public function getEmail () {
		return $this->_email;
	}
	
	/**
	 * Establece el email del usuario
	 *
	 * @param string $email
	 * @throws LogicException 
	 */
	public function setEmail ( $email ) {
		$this->_email = Utils::forzarEMailValido($email);	
	}
	
	/**
	 * Devuelve el nombre del usuario
	 *
	 * @return string 
	 */
	public function getNombre () {
		return $this->_nombre;
	}
	
	/**
	 * Establece el nombre del usuario
	 *
	 * @param string $nombre 
	 */
	public function setNombre ( $nombre ) {
		$this->_nombre = Utils::sanitizarString($nombre, true);
	}
	
	/**
	 * Establece el password del usuario
	 * 
	 * @param type $password 
	 */
	public function setPassword ( $password ) {
		$this->_pass = sha1($password);
	}
	
	/**
	 * Crea un nuevo objecto Usuario basado en el email (opcional)
	 *
	 * @param string $email 
	 */
	public function __construct( $email = null ) {
		if ( $email !== null ) {
			$this->setEmail($email);
			$this->_retrieve();
		}
	}
	
	
	private function _retrieve () {
		$sql = 'SELECT * FROM ' . self::TABLE . ' WHERE email = :email';
		$row = Database::getInstance()->getRow($sql, array(':email' => $this->_email));
		if ( $row && count($row) > 0 ) {
			$this->_id = $row['id'];
			$this->_email = $row['email'];
			$this->_nombre = $row['nombre'];
			$this->_created = $row['created'];
			$this->_modified = $row['modified'];
		} else {
			$this->save();
		}
	}
	
	
	public function save() {
		$sql = '';
		$params = array();
		$pass = $this->_pass;
		if ( $this->getID() ) {
			$sql .= ' UPDATE ' . self::TABLE . ' SET ';
			$sql .= ' email	= :email, nombre = :nombre ';
			if ( !empty($pass) ) {
				$sql .= ', pass = :pass';
				$params[':pass'] = $pass;
			}
			$params[':email'] =  $this->getEmail();
			$params[':nombre'] = $this->getNombre();
			$sql .= ' WHERE id = :id ';
			$params[':id'] = $this->getID();
			Database::getInstance()->execute($sql, $params);
		} else {
			$sql  = ' INSERT INTO ' . self::TABLE . ' (email, nombre';
			if ( !empty($pass) ) $sql .= ', pass ';
			$sql .= ') VALUES (:email, :nombre ';
			$params[':email'] =  $this->getEmail();
			$params[':nombre'] = $this->getNombre();
			if ( !empty($pass) ){
				$sql .= ', :pass ';
				$params[':pass'] = $pass;
			}
			$sql .= ') ';
			$this->_id = Database::getInstance()->execute($sql, $params, true);
		}
		return true;
	}
	
	/**
	 * Retorna un usuario por su id
	 *
	 * @param int $id
	 * @return Usuario 
	 */
	public static function getUsuarioById ($id) {
		$sql = 'SELECT * FROM ' . self::TABLE . ' WHERE id = :id';
		$row = Database::getInstance()->getRow($sql, array(':id' => $id));
		if ( $row && count($row) > 0 ) {
			$u = new Usuario();
			$u->_id = $row['id'];
			$u->setEmail($row['email']);
			$u->setNombre($row['nombre']);
			$u->_created = $row['created'];
			$u->_modified = $row['modified'];
			return $u;
		}
		return null;
	}
	
}
