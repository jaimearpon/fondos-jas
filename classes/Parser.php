<?php

require_once("Log.php");

class Parser {

	private static $_instance = null;
	private $_conn = null;
	private $_data;

	private $_afps;
	private $_fondos;

	private function __construct() {
		$this->_data = array();
		$this->_afps = array();
		$this->_fondos = array();
		$this->_conn = pg_connect("host=fondosafp.cl dbname=fondos user=afp_user password=afpAFP123") or die("Cannot connect to DB");
	}

	/**
	 * Get the instance (singleton)
	 * @return Parser The instance of this class
	 */
	public static function getInstance() {
		if (!isset(self::$_instance) || self::$_instance == null) {
			$c = __CLASS__;
			self::$_instance = new $c();
		}
		return self::$_instance;
	}

	private function getAFPID($afp) {
		if (!array_key_exists($afp, $this->_afps)) {
			$sql = "SELECT afp_id FROM afp WHERE nombre = '$afp'";
			$result = @pg_query($this->_conn, $sql);
			if (is_resource($result)) {
				$num_rows = @pg_num_rows($result);
				if ($num_rows == 1) {
					$row = @pg_fetch_array($result);
					$afpID = $row['afp_id'];
					$this->_afps[$afp] = $afpID;
				}
				@pg_freeresult($result);
			}
		}
		return $this->_afps[$afp];
	}

	private function getFondoID($fondo) {
		if (!array_key_exists($fondo, $this->_fondos)) {
			$sql = "SELECT fondo_id FROM fondo WHERE nombre = '$fondo'";
			$result = @pg_query($this->_conn, $sql);
			if (is_resource($result)) {
				$num_rows = @pg_num_rows($result);
				if ($num_rows == 1) {
					$row = @pg_fetch_array($result);
					$fondoID = $row['fondo_id'];
					$this->_fondos[$fondo] = $fondoID;
				}
				@pg_freeresult($result);
			}
		}
		return $this->_fondos[$fondo];
	}

	/**
	 * Realiza la captura de datos desde la superintendencia de pensiones a partir del informe generado
	 *
	 * @param int $year
	 * @return array
	 */
	public function parseCuotas($year, $retry = false) {
		ini_set('max_execution_time', 0);
		$added = 0;
		$fondos = array('A', 'B', 'C', 'D', 'E');
		$curdate = date("Ymd");
		$data = array();
		$min_date = date("Y-m-d", strtotime("-7 days"));
		if (!$retry) {
			//$min_date = "$year-12-01";
		}
		$sql = '';
		foreach ($fondos as $fondo) {
			$url = "http://www.safp.cl/apps/vcuofon/vcfAFPxls.php?aaaaini=$year&aaaafin=$year&tf=$fondo&fecconf=$curdate";
			Log::getInstance()->log($url);
			$c = curl_init();
			curl_setopt($c, CURLOPT_URL, $url);
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($c);
			$responseInfo = curl_getinfo($c);
			curl_close($c);
			if (intval($responseInfo['http_code']) == 200) {
				Log::getInstance()->log("[parser] Procesando datos para el fondo $fondo desde la fecha $min_date");
				$data[$fondo] = $this->processResponse($response, $min_date);
				//Log::getInstance()->log_r($data);
				foreach ($data[$fondo] as $fecha => $afps) {
					$added += $this->storeData($fondo, $fecha, $afps);
				}
			} else {
				$errorMessage = "No se ha podido obtener información para el fondo $fondo: " . print_r($responseInfo);
				AmazonSES::getInstance()->sendEmail('julio.araya@fondosafp.cl', AWS_EMAIL_CONTACTO, 'Error al actualizar las cuotas', $errorMessage);
			}
		}

		if ($added == 0 && $retry) {
			Log::getInstance()->log("[parser] Probando con año anterior");
			return $this->parseCuotas((int)$year - 1, false);
		}
		
		return $added;
	}

	private function processResponse($response, $minDate) {
		$lines = explode("\n", $response);
		foreach ($lines as $k => $line) {
			$line = trim(str_replace('.', '', $line));
			$lines[$k] = str_replace(',', '.', $line);
			if (strlen(trim($line)) == 0) {
				unset($lines[$k]);
			}
		}

		$lines = array_values($lines);
		$linea_afp = explode(';', $lines[0]);
		$afps = array();
		$data = array();
		for ($i = 0; $i < count($linea_afp); $i++) {
			$afp = trim($linea_afp[$i]);
			if ($afp != 'Fecha' && $afp != '') {
				$afps["$i"] = $afp;
			}
		}

		for ($i = 2; $i < count($lines); $i++) {
			$campos = explode(';', $lines[$i]);
			foreach ($afps as $pos => $afp) {
				if (array_key_exists($pos, $campos)) {
					$fecha = trim($campos[0]);
					if ($fecha !== 'Fecha' && strlen($fecha) > 0) {
						if ($fecha >= $minDate) {
							//Log::getInstance()->log("Entra $fecha >= $minDate");
							$data[$fecha][$afp] = array(
								'valor' => $campos[$pos],
								'patrimonio' => $campos[(int) $pos + 1]
							);
							//Log::getInstance()->log("$fecha: " . print_r($data[$fecha], true));
						} else {
							//Log::getInstance()->log("Falla $fecha >= $minDate");
						}
					} else {
						Log::getInstance()->log("[parser] Error en linea $i, se esperaba fecha, se tiene: '$fecha'");
					}
				}
			}
		}
		return $data;
	}

	private function storeData($fondo, $fecha, $afps) {
		$updated = 0;
		$sql = '';
		foreach ($afps as $afp => $data) {
			$sql .= $this->registerCuota($fecha, $afp, $fondo, $data['valor'], $data['patrimonio']);
		}
		if (strlen($sql) > 0) {
			$sql .= "; commit;";
			//Log::getInstance()->log($sql);
			$result = @pg_query($this->_conn, $sql);
			if (is_resource($result)) {
				$updated = pg_affected_rows($result);
			}
		}
		return $updated;
	}

	private function alreadyRegistered($fecha, $afp, $fondo) {
		$sqlExists = " SELECT cuota_id FROM cuotas WHERE fecha = '$fecha' ";
		$sqlExists .= " AND afp_id = " . $this->getAFPID($afp);
		$sqlExists .= " AND fondo_id = " . $this->getFondoID($fondo);
		$result = @pg_query($this->_conn, $sqlExists);
		$cuotaID = 0;
		if (is_resource($result)) {
			$num_rows = @pg_num_rows($result);
			if ($num_rows == 1) {
				$row = @pg_fetch_array($result);
				$cuotaID = $row['cuota_id'];
			} else {
				$cuotaID = 0;
			}
			@pg_freeresult($result);
		}
		return $cuotaID;
	}

	private function registerCuota($fecha, $afp, $fondo, $valorCuota, $valorPatrimonio) {
		if ($valorCuota <= 0)
			return '';
		//Log::getInstance()->log($afp);
		$afpId = $this->getAFPID($afp);
		$fondoId = $this->getFondoID($fondo);
		$sql = '';
		$cuotaId = $this->alreadyRegistered($fecha, $afp, $fondo);
		if ($cuotaId === 0) {
			$sql = " INSERT INTO cuotas (fecha, afp_id, fondo_id, valor, patrimonio) VALUES('$fecha', $afpId, $fondoId, $valorCuota, $valorPatrimonio);\n ";
		} else {
			$sql = " UPDATE cuotas SET  valor = $valorCuota, patrimonio = $valorPatrimonio WHERE cuota_id = $cuotaId AND fecha = '$fecha';\n ";
		}

		return $sql;

//		$result = @pg_query($this->_conn, $sql);
//		if (is_resource($result)) {
//			return @pg_affected_rows($result) > 0;
//		}
//		return false;
	}

}
