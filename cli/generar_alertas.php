<?php
require_once dirname(__FILE__).'/../config/config.php';
$alertas_generadas = Alerta::generarEmailDeAlertas();
if ($alertas_generadas > 0) {
	Log::getInstance()->log("$alertas_generadas alertas generadas");
}