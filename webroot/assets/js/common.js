$(document).ready(function() {
	$("#desde").datepicker({dateFormat: "yy-mm-dd"});
	$("#hasta").datepicker({dateFormat: "yy-mm-dd"});
	$('.container-body').css('min-height', $(window).height() - 290);
	$('#aditional_filters a').click(function(){
		$('#aditional_filters div').fadeToggle(50, function(){
			if($('#aditional_filters div').is(':visible')){
				$('#aditional_filters a').html('[-] menos opciones');
			} else {
				$('#aditional_filters a').html('[+] mas opciones');
			}
		});
	});
});
function emailValido(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

function crearAlerta() {
	$('.top_message').hide();
	if ($('#valor').val() == '') {
		errorMessage('Por favor ingresa un valor');
		return false;
	}
	
	if ($('#email').val() == '') {
		errorMessage('Por favor ingresa tu email');
		return false;
	} else if (!emailValido($('#email').val())) {
		errorMessage('Por favor ingresa un email válido');
		return false;
	}
	
	$.ajax({
		type: 'POST',
		cache: false,
		url: '/ajax/crear_alerta/',
		data: $('#form_alertas').serialize(),
		dataType: 'json',
		success: function(response) {
			_hideOverlay();
			$('#btn_crear_alerta').removeAttr('disabled');
			if (response.result == 'OK') {
				$('#valor').val('');
				successMessage(response.message);
			} else if(response.result == 'INFO') {
				infoMessage(response.message);
			} else {
				errorMessage(response.error);
			}
		},
		beforeSend: function() {
			_showOverlay();
			$('#btn_crear_alerta').attr('disabled', 'disabled');
		},
		error: function(a, b, c) {
			_ajaxError(a, b, c);
			_hideOverlay();
			$('#btn_crear_alerta').removeAttr('disabled');
		}
	});
	return false;
}

function _ajaxError(j, ts, et) {
	var e = 'Ha ocurrido un error';
	if (ts != null)
		e += ' (' + ts + ')';
	if (et != null)
		e += ': ' + et;
	errorMessage(e);
}

function _showOverlay() {
	$('.ajax-loader').height($(window).height()).show(0);
}

function _hideOverlay() {
	$('.ajax-loader').hide(0);
}

var mostrando = 0;

function _top_message (msg, type) {
	$('.top_message')
		.hide(0)
		.removeClass('info-message')
		.removeClass('error-message')
		.removeClass('success-message')
		.html('<div>'+msg+'</div>')
		.addClass(type)
		.fadeIn(300)
		.delay(2000)
		.fadeOut(100, function(){$(this).removeClass(type);});
	return false;
}

function successMessage (msg) {
	_top_message(msg,'success-message');
}

function infoMessage (msg) {
	_top_message(msg,'info-message');
}

function errorMessage ( msg ) {
	_top_message(msg,'error-message');
}