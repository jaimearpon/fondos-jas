$(function() {
	createChart();
	updateChart();
	Highcharts.setOptions({
		lang: {
			months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']
		}
	});

});

function updateChart() {
	var seriesOptions = [], names = [];
	$('input.data_fondos:checked').each(function(){names.push($(this).val());});
	if (names.length === 0) {
		errorMessage('Tienes que seleccionar al menos un fondo');
		return false;
	}
	createChart();
	$('#afp_name').html('');
	$('#periodo').html('');
	$('.box-rentabilidad').remove();
	$.getJSON('/api/1.0/fondos.json?afp=' + $('#afp').val() 
			+ '&fondos='+names 
			+ '&from='+$('#desde').val()
			+ '&until='+$('#hasta').val() 
			+ '&type='+$('#sel_tipo').val()
			+ '&hideFDS='+$('#ocultarFDS').val()
			+ '&callback=?', function(r) {	
		var c=0;
		var count = names.length;
		var sel_tipo = $('#sel_tipo').val();
		for(var i in r.fondos){
			var fondo = r.fondos[i];
			var color = 'red';
			if (parseFloat(fondo['variacion_real'])>=0) {
				color = 'green';
			}
			seriesOptions[c++]={name:fondo.descripcion,data:fondo.data,type:'spline',shadow:true};
			if (sel_tipo == 'porcentaje') {
				$('.details').append('<div class="box-rentabilidad '+(count===1?'box-center':'')+'" >\n\
					<label class="bold">'+fondo['descripcion']+'</label> <span class="'+color+'">'+fondo['variacion_real']+'%</span>\n\
					<label>Desde '+fondo['fecha_valor_inicial']+'</label>\n\
					<label>Hasta '+fondo['fecha_valor_final']+'</label>\n\
				</div>');
			} else if (sel_tipo == 'patrimonio') {
				$('.details').append('<div class="box-rentabilidad '+(count===1?'box-center':'')+'" >\n\
					<label class="bold">'+fondo['descripcion']+'</label> <span class="'+color+'">$'+fondo['variacion_real']+' ('+fondo['variacion_porcentual']+' %)</span>\n\
					<label>BB $'+fondo['valor_inicial']+' el '+fondo['fecha_valor_inicial']+'</label>\n\
					<label>BB $'+fondo['valor_final']+' el '+fondo['fecha_valor_final']+'</label>\n\
				</div>');
			} else {
				$('.details').append('<div class="box-rentabilidad '+(count===1?'box-center':'')+'" >\n\
					<label class="bold">'+fondo['descripcion']+'</label> <span class="'+color+'">$'+fondo['variacion_real']+' ('+fondo['variacion_porcentual']+' %)</span>\n\
					<label>$'+fondo['valor_inicial']+' el '+fondo['fecha_valor_inicial']+'</label>\n\
					<label>$'+fondo['valor_final']+' el '+fondo['fecha_valor_final']+'</label>\n\
				</div>');
			}
			$('#afp_name').html(r.afp);
			$('#periodo').html(r.periodo);
		}
		refreshChart(seriesOptions,r.description);
	});
	return false;
}
	

// create the chart when all data is loaded
function createChart(seriesOptions) {
	$('#chart_container').highcharts('StockChart', {
		chart: {
			backgroundColor: {
			   linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
			   stops: [[0, 'rgb(255, 255, 255)'],[1, 'rgb(240, 240, 255)']]
			},
			borderWidth: 1,
			plotBackgroundColor: 'rgba(255, 255, 255, .9)',
			plotShadow: true,
			plotBorderWidth: 1
		},
		rangeSelector: {
			buttons: [
				{type: 'month',count:1,text:'1m'}, 
				{type: 'month',count: 3,text: '3m'}, 
				{type: 'month',count: 6,text: '6m'}, 
				{type: 'year',count: 1,text: '1y'}, 
				{type: 'all', text: 'All'}
			],
			inputEnabled: false,
			selected: 4
		},

		yAxis: {
			minorTickInterval: 'auto',
			lineColor: '#000',
			lineWidth: 1,
			tickWidth: 1,
			tickColor: '#000',
			labels: {
				formatter: function() { return '$' + this.value;},
			},
		},
		title : {text : 'Fondos AFP - https://www.fondosafp.com'},
		tooltip: {
			xDateFormat: '%A %e de %B del %Y',
			pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>'+($('#sel_tipo').val()!='porcentaje' ? '$':'')+'{point.y}'+($('#sel_tipo').val()=='porcentaje' ? '%':'')+'</b><br/>',
			valueDecimals: 2
		},
		series: seriesOptions
	});

	var chart = $('#chart_container').highcharts();
	chart.showLoading();
	return false;
}

function refreshChart(seriesOptions, subtitle){
	createChart(seriesOptions);
	var chart = $('#chart_container').highcharts();
	chart.setTitle(chart.options.title,{text:subtitle});
	chart.hideLoading();
	return false;
}