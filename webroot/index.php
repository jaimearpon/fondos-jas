<?php

require_once '../config/config.php';
		
$info = array();
$info['capital'] = array();
$info['cuprum'] = array();
$info['habitat'] = array();
$info['modelo'] = array();
$info['planvital'] = array();
$info['provida'] = array();
$fondos = array('A','B','C','D','E');
foreach ( $info as $afp => $data) {
	foreach ( $fondos as $fondo ) {
		$info[$afp][$fondo] = array(
			'12_meses' => Cuota::getPorcentage(strtotime("-1 year"), $afp, $fondo, "12-meses"),
			'6_meses' => Cuota::getPorcentage(strtotime("-6 months"), $afp, $fondo, "6-meses"),
			'3_meses' => Cuota::getPorcentage(strtotime("-3 months"), $afp, $fondo, "3-meses"),
			'este_mes' => Cuota::getPorcentage(mktime (0, 0, 0, date("n"), 1), $afp, $fondo, "este-mes"),
			'este_anyo' => Cuota::getPorcentage(mktime (0, 0, 0, 1, 1, date("Y")), $afp, $fondo, "este_anyo")
		);
	}	
}

if ( isset($_GET['notrack']) ) {
	$_SESSION['notrack'] = 'enabled';
}

foreach ( $info as $afp => $data)
	$smarty->assign('tabla_'.$afp, Utils::renderTableFondos($afp, $data));

$smarty->assign('porcentajes', $info);
$smarty->assign('page', array());
$smarty->assign('mtime_js_commons', filemtime(dirname(__FILE__).DS.'assets'.DS.'js'.DS.'common.js'));
$smarty->assign('mtime_css_main', filemtime(dirname(__FILE__).DS.'assets'.DS.'css'.DS.'main.css'));
$smarty->assign('query_count', Database::getInstance()->getQueryCount());
$smarty->assign('elapsed_time', Database::getInstance()->getElapsedTime());
$smarty->display('home.tpl');
