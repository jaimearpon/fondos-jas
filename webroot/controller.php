<?php
@ob_clean();
require_once '../config/config.php';

$uri = $_SERVER['REQUEST_URI'];
$controller = '';
if( strpos($uri, '/ajax') === 0) {
	$controller = 'ajax.php';
} elseif ( strpos($uri, '/fondos') === 0) {
	$controller = 'fondos.php';
} elseif ( strpos($uri, '/alertas') === 0) {
	$controller = 'alertas.php';
}/* elseif ( strpos($uri, '/contacto') === 0) {
	$controller = 'contacto.php';
} elseif ( strpos($uri, '/about') === 0) {
	$controller = 'about.php';
} */elseif (strpos($uri, '/blog') === 0) {
	$controller = 'blog.php';
} elseif (strpos($uri, '/api') === 0) {
	$controller = 'api.php';
}

unset ($uri);
if ( strlen($controller) > 0 ) {
	try {
		if ( isset($_GET['notrack']) ) {
			$_SESSION['notrack'] = 'enabled';
		}
		require_once CONTROLLER_DIR.$controller;
	} catch ( Exception $ex ) {
		$smarty->assign('error', $ex->getMessage());
		$smarty->assign('trace', $ex->getTraceAsString());
		require CONTROLLER_DIR.'500.php';
	}
} else {
	require CONTROLLER_DIR.'404.php';
}