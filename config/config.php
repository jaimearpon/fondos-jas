<?php

//Classes Autoload
spl_autoload_register('application_autoload');

function application_autoload($className){
  	$fileName = loadClass($className, CLASSES_DIR);
  	if (file_exists($fileName))
  		require_once $fileName;
}

function loadClass($className, $baseDir){
	$dirHandle = opendir($baseDir);
	$theFile = false;
	while ( ($file = readdir($dirHandle)) != false ) {
		if ( $file != '.' && $file != '..' ) {
			if( is_dir($baseDir.$file) ){
				$theFile = loadClass($className, $baseDir.$file.DS);
				if ( $theFile ) {
					return $theFile;
				}
			} else {
				$base = basename($file);
				if ( $base === $className.'.php' ) {
					closedir($dirHandle);
					return $baseDir.DS.$file;
				} elseif ( $base == $className.'.class.php') {
					closedir($dirHandle);
					return $baseDir.DS.$file;
				}
			}
		}
	}
	closedir($dirHandle);
	return $theFile;
}

function isCli() {
	return php_sapi_name() == 'cli';
}

function isAPI() {
	return strpos($_SERVER['REQUEST_URI'], '/api') !== FALSE;
}

/**
 * Indica si se está ejecutando en modo consola
 */
define('IS_CLI', isCli());
define('IS_API', isAPI());

define('DS', DIRECTORY_SEPARATOR);
define('WEBROOT_DIR'   , dirname(__FILE__).DS );
define('CODEROOT'      , dirname(dirname(__FILE__)).DS );
define('CLASSES_DIR'   , CODEROOT.'classes'.DS);
define('CONFIG_DIR'    , CODEROOT.'configs'.DS);
define('LIBS_DIR'      , CODEROOT.'libs'.DS);
define('LOGS_DIR'      , CODEROOT.'logs'.DS);
define('MEDIA_DIR'     , WEBROOT_DIR.'media'.DS);
define('CONTROLLER_DIR', CODEROOT.'controllers'.DS);
define('LIBCHART_DIR'  , LIBS_DIR.'libchart'.DS.'classes'.DS);

//Amazon Web Services
define('AWS_EMAIL_ALERT'   , 'alertas@fondosafp.cl');
define('AWS_EMAIL_CONTACTO', 'contacto@fondosafp.cl');

define('IS_SSL', array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS'] === 'on');

if (!IS_CLI) {
	define('SERVER_URL', 'http'.(IS_SSL?'s':'').'://'.$_SERVER['HTTP_HOST'].'/' );
	define('SERVER_URI', 'http'.(IS_SSL?'s':'').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
	define('CHART_URL'     , SERVER_URL.'assets/charts/');
	define('CHART_DIR'     , SERVER_URL.'assets'.DS.'charts'.DS);
	session_start();
	if (!IS_API) {
		header('Content-type: text/html; charset=utf-8');
		
		//Smarty Template Engine
		define('SMARTY_DIR'         , LIBS_DIR.'smarty'.DS.'3.1.14'.DS.'libs'.DS);
		define('SMARTY_TEMPLATE_DIR', CODEROOT.'templates'.DS);
		define('SMARTY_COMPILE_DIR' , CODEROOT.'templates_c'.DS);
		define('SMARTY_CONFIG_DIR'  , CODEROOT.'config'.DS);
		define('SMARTY_CACHE_DIR'   , CODEROOT.'cache'.DS);

		//Load the smarty Engine
		require_once SMARTY_DIR.'Smarty.class.php';

		$smarty = new Smarty();
		$smarty->template_dir = SMARTY_TEMPLATE_DIR;
		$smarty->compile_dir  = SMARTY_COMPILE_DIR;
		$smarty->config_dir   = SMARTY_CONFIG_DIR;
		$smarty->cache_dir    = SMARTY_CACHE_DIR;
	}
}