CREATE TABLE afp (
  afp_id serial NOT NULL,
  nombre character varying(30) NOT NULL,
  descripcion character varying(128),
  CONSTRAINT pk_afp PRIMARY KEY (afp_id)
) WITH ( OIDS=FALSE );
ALTER TABLE afp OWNER TO afp_user;

CREATE TABLE fondo (
  fondo_id serial NOT NULL,
  nombre character(1) NOT NULL,
  descripcion character varying(128),
  CONSTRAINT pk_fondo PRIMARY KEY (fondo_id)
) WITH ( OIDS=FALSE );
ALTER TABLE fondo OWNER TO afp_user;

CREATE TABLE cuotas (
  cuota_id serial NOT NULL,
  fecha character varying(10) NOT NULL,
  afp_id bigint NOT NULL,
  fondo_id bigint NOT NULL,
  valor double precision NOT NULL,
  CONSTRAINT pk_cuotas PRIMARY KEY (cuota_id),
  CONSTRAINT fk_afp FOREIGN KEY (afp_id)
      REFERENCES afp (afp_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_fondo FOREIGN KEY (fondo_id)
      REFERENCES fondo (fondo_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_reg UNIQUE (afp_id, fondo_id, fecha)
) WITH ( OIDS=FALSE );
ALTER TABLE cuotas OWNER TO afp_user;

CREATE TABLE usuario (
  id serial NOT NULL,
  email character varying(255) NOT NULL,
  nombre character varying(255) DEFAULT NULL::character varying,
  pass character(40) DEFAULT NULL::bpchar,
  created timestamp without time zone NOT NULL DEFAULT now(),
  modified timestamp without time zone NOT NULL DEFAULT now(),
  CONSTRAINT usuario_pk PRIMARY KEY (id)
) WITH ( OIDS=FALSE );
ALTER TABLE usuario OWNER TO afp_user;

CREATE TABLE alerta (
  id serial NOT NULL,
  usuario_id integer NOT NULL,
  afp_id integer NOT NULL,
  fondo_id integer NOT NULL,
  tipo smallint NOT NULL,
  valor real NOT NULL,
  created timestamp without time zone NOT NULL DEFAULT now(),
  modified timestamp without time zone NOT NULL DEFAULT now(),
  enviada boolean NOT NULL DEFAULT false,
  CONSTRAINT alert_pk PRIMARY KEY (id),
  CONSTRAINT alerta_fk_usuario_id FOREIGN KEY (usuario_id) REFERENCES usuario (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT alerta_fk_afp_id     FOREIGN KEY (afp_id)     REFERENCES afp (afp_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT alerta_fk_fondo_id   FOREIGN KEY (fondo_id)  REFERENCES fondo (fondo_id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
) WITH ( OIDS=FALSE );
ALTER TABLE alerta OWNER TO afp_user;

CREATE TABLE email_queue (
	id		serial NOT NULL,
	created		timestamp without time zone NOT NULL DEFAULT now(),
	status		CHAR(12) NOT NULL DEFAULT 'none',
	sent		timestamp without time zone NULL,
	de		CHAR(250) NOT NULL,
	para		CHAR(250) NOT NULL,
	subject		CHAR(250) NOT NULL,
	body		TEXT NOT NULL,
	CONSTRAINT email_queue_pk PRIMARY KEY (id)
) WITH ( OIDS=FALSE );
ALTER TABLE email_queue OWNER TO afp_user;


create type variacion as (fondo_id integer, valor numeric, fecha CHAR(10) );

create or replace function VariacionPorcentual(text, text, int, int) returns setof variacion as
'
declare
r variacion%rowtype;
v numeric;
begin
select valor into v from cuotas where afp_id = $3 and fondo_id = $4 and fecha >= $1 and fecha <= $2;

for r in select fondo_id, valor, fecha from cuotas where fecha >= $1 AND fecha <= $2 AND afp_id = $3 AND fondo_id = $4 order by fecha asc loop
  r.valor = round((r.valor*100)/v - 100,2);
  return next r;
end loop;
return;
end
'
language 'plpgsql';

--Uso: select * from VariacionPorcentual('2012-01-01', '2012-02.01',3,1)